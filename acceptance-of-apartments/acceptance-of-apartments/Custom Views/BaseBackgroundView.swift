//
//  BaseBackgroundView.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 27/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class BaseBackgroundView: UIView {
    
    private weak var backgroundImageView: UIImageView?
    
    override var backgroundColor: UIColor? {
        didSet {
            backgroundImageView?.isHidden = backgroundColor != nil
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }

}

private extension BaseBackgroundView {
    
    func setup() {
        let backgroundImage = UIImageView(image: #imageLiteral(resourceName: "background"))
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.frame = bounds
        backgroundImage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(backgroundImage, at: 0)
        backgroundImageView = backgroundImage
    }
    
}
