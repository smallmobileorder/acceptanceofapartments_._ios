//
//  PickerTextField.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 22/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class PickerTextField: UITextField {
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }

}
