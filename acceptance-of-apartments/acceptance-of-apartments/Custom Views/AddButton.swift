//
//  AddButton.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 13/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

@IBDesignable
class AddButton: UIButton {
    
    @IBInspectable var plusSignOffset: CGFloat = 8.0
    
    @IBInspectable var active: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var plusSignWidth: CGFloat = 3.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    override func draw(_ rect: CGRect) {
        tintColor.setStroke()
        
        let height = frame.height
        let width = frame.width
        
        let circleRect = UIBezierPath(ovalIn: CGRect(x: 0.5,
                                                     y: 0.5,
                                                     width: width - 1.0,
                                                     height: height - 1.0))
        circleRect.lineWidth = 1.0
        circleRect.stroke()
        
        if !active {
            tintColor.withAlphaComponent(0.25).setStroke()
        }
        
        let plusSign = UIBezierPath()
        plusSign.lineWidth = plusSignWidth
        plusSign.move(to: CGPoint(x: plusSignOffset, y: height / 2.0))
        plusSign.addLine(to: CGPoint(x: width - plusSignOffset, y: height / 2.0))
        plusSign.move(to: CGPoint(x: width / 2.0, y: plusSignOffset))
        plusSign.addLine(to: CGPoint(x: width / 2.0, y: height - plusSignOffset))
        plusSign.stroke()
    }

}
