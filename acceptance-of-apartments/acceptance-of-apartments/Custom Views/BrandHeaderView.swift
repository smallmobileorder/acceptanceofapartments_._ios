//
//  BrandHeaderView.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 21/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

@IBDesignable
class BrandHeaderView: UIView {
    
    @IBInspectable var image: UIImage? {
        didSet {
            imageView.image = image
            setNeedsDisplay()
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var imageHeightOffset: CGFloat = 14.0 {
        didSet {
            imageViewHeightConstraint.constant = -imageHeightOffset * 2.0
        }
    }
    
    @IBInspectable var imageOffset: CGPoint = .zero {
        didSet {
            imageViewCenterXConstraint.constant = imageOffset.x
            imageViewCenterYConstraint.constant = imageOffset.y
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric,
                      height: statusBarHeight + 76.0)
    }
    
    private var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
    
    private let lineWidth: CGFloat = 2.0
    
    private weak var containerView: UIView!
    private weak var imageViewHeightConstraint: NSLayoutConstraint!
    private weak var imageViewCenterXConstraint: NSLayoutConstraint!
    private weak var imageViewCenterYConstraint: NSLayoutConstraint!
    private weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func draw(_ rect: CGRect) {
        guard image != nil else { return }
        
        tintColor.setStroke()
        
        let circleCenter = CGPoint(x: containerView.frame.midX,
                                   y: containerView.frame.midY)
        
        let radius = (containerView.frame.height - lineWidth) / 2.0
        let circlePath = UIBezierPath(arcCenter: circleCenter,
                                      radius: radius,
                                      startAngle: 0.0,
                                      endAngle: 2.0 * .pi,
                                      clockwise: true)
        circlePath.lineWidth = lineWidth
        circlePath.stroke()
        
        let midY = containerView.frame.midY
        let linePath = UIBezierPath()
        linePath.lineWidth = lineWidth
        linePath.move(to: CGPoint(x: 0.0, y: midY))
        linePath.addLine(to: CGPoint(x: circleCenter.x - radius, y: midY))
        linePath.move(to: CGPoint(x: circleCenter.x + radius, y: midY))
        linePath.addLine(to: CGPoint(x: frame.width, y: midY))
        linePath.stroke()
    }
    
}

private extension BrandHeaderView {
    
    func setup() {
        backgroundColor = .clear
        contentMode = .redraw
        
        let containerView = UIView()
        containerView.backgroundColor = .clear
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        let containerTopConstraint = containerView.topAnchor.constraint(equalTo: topAnchor, constant: statusBarHeight)
        NSLayoutConstraint.activate([
            containerTopConstraint,
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        self.containerView = containerView
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView)
        
        let heightConstraint = imageView.heightAnchor.constraint(equalTo: containerView.heightAnchor, constant: -imageHeightOffset * 2.0)
        let centerXConstraint = imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        let centerYConstraint = imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        NSLayoutConstraint.activate([
            centerXConstraint,
            centerYConstraint,
            heightConstraint,
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
        ])
        imageViewCenterXConstraint = centerXConstraint
        imageViewCenterYConstraint = centerYConstraint
        imageViewHeightConstraint = heightConstraint
        self.imageView = imageView
        
        NotificationCenter.default.addObserver(forName: UIApplication.didChangeStatusBarFrameNotification,
                                               object: nil,
                                               queue: .main) { [weak self, weak containerTopConstraint] _ in
                                                guard let self = self else { return }
                                                
                                                containerTopConstraint?.constant = self.statusBarHeight
                                                self.invalidateIntrinsicContentSize()
        }
    }
    
}
