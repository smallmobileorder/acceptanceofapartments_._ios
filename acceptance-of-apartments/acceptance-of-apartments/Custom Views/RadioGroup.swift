//
//  RadioGroup.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 08/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

protocol RadioGroupDelegate: class {
    func radioGroup(_ radioGroup: RadioGroup, didSelectRadioButton: UIButton?)
}

@objc protocol Selectable {
    var setSelected: Bool { get set }
}

@objc protocol Descriptable {
    var name: String { get }
}

class RadioGroup {
    
    typealias RadioButton = UIButton & Selectable & Descriptable
    
    let radioButtons: [RadioButton]
    
    init(radioButtons: [RadioButton], selectFirst: Bool) {
        self.radioButtons = radioButtons
        
        if selectFirst, let first = radioButtons.first { select(activeButton: first) }
        
        for button in radioButtons { configure(button: button) }
    }
    
    weak var selectedButton: RadioButton?
    weak var delegate: RadioGroupDelegate?
    
    @objc func select(activeButton: RadioButton?) {
        activeButton?.setSelected = true
        selectedButton = activeButton
        for button in radioButtons where button !== selectedButton { button.setSelected = false }
        
        delegate?.radioGroup(self, didSelectRadioButton: activeButton)
    }
    
}

private extension RadioGroup {
    
    func configure(button: RadioButton) {
        button.addTarget(self, action: #selector(select(activeButton:)), for: .touchUpInside)
    }
    
}
