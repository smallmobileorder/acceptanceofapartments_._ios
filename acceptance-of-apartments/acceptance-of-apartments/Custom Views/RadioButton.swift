//
//  RadioButton.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 23/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class RadioButton: UIButton, Selectable {
    
    private var activeImage: UIImage!
    private var inactiveImage: UIImage!
    
    var setSelected: Bool = false {
        didSet {
            setImage(setSelected ? activeImage : inactiveImage, for: .normal)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
}

private extension RadioButton {
    
    func setup() {
        let imageHeight = frame.size.height * 0.9
        let imageSize = CGSize(width: imageHeight, height: imageHeight)
        
        let circleHeight = imageHeight / 2.0
        let circleSize = CGSize(width: circleHeight, height: circleHeight)
        let circleOffset = (imageHeight - circleHeight) / 2.0
        let circleRect = CGRect(origin: CGPoint(x: circleOffset, y: circleOffset), size: circleSize)
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.saveGState()
        
        context.setLineWidth(1.0)
        
        context.strokeEllipse(in: circleRect)
        
        inactiveImage = UIGraphicsGetImageFromCurrentImageContext()
        
        let filledCircleHeight = circleHeight / 2.0
        let filledCircleSize = CGSize(width: filledCircleHeight, height: filledCircleHeight)
        let filledCircleOffset = circleOffset + (circleHeight - filledCircleHeight) / 2.0
        let filledCircleRect = CGRect(origin: CGPoint(x: filledCircleOffset, y: filledCircleOffset), size: filledCircleSize)
        
        context.fillEllipse(in: filledCircleRect)
        
        context.restoreGState()
        
        activeImage = UIGraphicsGetImageFromCurrentImageContext()
        
        setSelected = false
    }
    
}

