//
//  DatePickerTextField.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 03/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class DatePickerTextField: PickerTextField {
    
    var format = "dd.MM.yyyy" {
        didSet {
            update()
        }
    }
    
    var date: Date? {
        didSet {
            update()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
}

private extension DatePickerTextField {
    
    func setup() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePicked(datePicker:)), for: .valueChanged)
    }
    
    func update() {
        if let date = date {
            text = date.formatted(format)
        } else {
            text = nil
        }
    }
    
    @objc func datePicked(datePicker: UIDatePicker) {
        date = datePicker.date
    }
    
}
