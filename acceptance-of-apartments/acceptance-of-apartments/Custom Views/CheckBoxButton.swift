//
//  CheckBoxButton.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 13/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class CheckBoxButton: UIButton {
    
    private let checkMarkOffset: CGFloat = 4.0
    
    var marked: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var filled: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        guard marked || filled else { return }
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.setFillColor(tintColor.cgColor)
        context.fill(rect)
        
        guard marked else { return }
        
        let size = min(frame.width, frame.height)
        
        let startPoint = CGPoint(x: checkMarkOffset,
                                 y: size / 2.0)
        let middlePoint = CGPoint(x: size / 2.3,
                                  y: size * 0.9 - checkMarkOffset)
        let endPoint = CGPoint(x: size - checkMarkOffset * 1.1,
                               y: checkMarkOffset * 2.0)
        
        context.setStrokeColor(UIColor.white.cgColor)
        context.setLineWidth(3.0)
        context.move(to: startPoint)
        context.addLine(to: middlePoint)
        context.addLine(to: endPoint)
        context.strokePath()
    }
    
    override func tintColorDidChange() {
        layer.borderColor = tintColor.cgColor
    }

}
