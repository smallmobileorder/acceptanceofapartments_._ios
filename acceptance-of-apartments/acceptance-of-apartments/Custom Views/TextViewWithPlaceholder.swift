//
//  TextViewWithPlaceholder.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 19/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

@IBDesignable
class TextViewWithPlaceholder: UITextView {
    
    @IBInspectable var placeholder: String = ""
    @IBInspectable var placeholderColor: UIColor = .black {
        didSet {
            if isPlaceholdered { addPlaceholder() }
        }
    }
    
    @IBInspectable var contentColor: UIColor = .black
    
    private var isPlaceholdered: Bool = false {
        didSet {
            if isPlaceholdered {
                addPlaceholder()
            } else {
                removePlaceholder()
            }
        }
    }
    
    override var text: String! {
        get {
            return isPlaceholdered ? "" : super.text
        }
        set {
            super.text = newValue ?? ""
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        isPlaceholdered = text.isEmpty
    }
    
    func set(text: String) {
        isPlaceholdered = text.isEmpty
        if !text.isEmpty {
            self.text = text
        }
    }
    
}

private extension TextViewWithPlaceholder {
    
    func setup() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidBeginEditing),
                                               name: UITextView.textDidBeginEditingNotification,
                                               object: self)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidEndEditing),
                                               name: UITextView.textDidEndEditingNotification,
                                               object: self)
    }
    
    @objc func textDidBeginEditing() {
        isPlaceholdered = false
    }
    
    @objc func textDidEndEditing() {
        isPlaceholdered = text.isEmpty
    }
    
    func addPlaceholder() {
        textColor = placeholderColor
        text = placeholder
    }
    
    func removePlaceholder() {
        if textColor === placeholderColor {
            textColor = contentColor
            text = ""
        }
    }
    
}
