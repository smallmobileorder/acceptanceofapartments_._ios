//
//  Style.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 14/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

struct Style {
    
    static func tintColor(for category: String) -> UIColor? {
        return tintColor(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func tintColor(for type: GraphCategory.CategoryType?) -> UIColor? {
        guard let type = type else { return nil }
        switch type {
        case .door: return UIColor(red: 162, green: 71, blue: 63)
        case .furnish: return UIColor(red: 213, green: 146, blue: 130)
        case .windows: return UIColor(red: 206, green: 161, blue: 94)
        case .facade: return UIColor(red: 149, green: 175, blue: 92)
        case .stainedGlass: return UIColor(red: 187, green: 202, blue: 204)
        case .plumbing: return UIColor(red: 142, green: 149, blue: 190)
        case .electricity: return UIColor(red: 135, green: 101, blue: 143)
        case .parking: return UIColor(red: 12, green: 71, blue: 238)
        }
    }
    
    static func strokeColor(for category: String) -> UIColor? {
        return strokeColor(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func strokeColor(for type: GraphCategory.CategoryType?) -> UIColor? {
        guard let type = type else { return nil }
        switch type {
        case .door: return UIColor(red: 147, green: 35, blue: 26)
        case .furnish: return UIColor(red: 204, green: 95, blue: 68)
        case .windows: return UIColor(red: 218, green: 185, blue: 136)
        case .facade: return UIColor(red: 149, green: 175, blue: 92)
        case .stainedGlass: return UIColor(red: 187, green: 202, blue: 204)
        case .plumbing: return UIColor(red: 142, green: 149, blue: 190)
        case .electricity: return UIColor(red: 118, green: 78, blue: 128)
        case .parking: return UIColor(red: 12, green: 71, blue: 238)
        }
    }
    
    static func textColor(for category: String) -> UIColor? {
        return textColor(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func textColor(for type: GraphCategory.CategoryType?) -> UIColor? {
        guard let type = type else { return nil }
        switch type {
        case .door: return UIColor(red: 147, green: 35, blue: 26)
        case .furnish: return UIColor(red: 204, green: 95, blue: 68)
        case .windows: return UIColor(red: 179, green: 137, blue: 75)
        case .facade: return UIColor(red: 128, green: 151, blue: 79)
        case .stainedGlass: return UIColor(red: 84, green: 152, blue: 161)
        case .plumbing: return UIColor(red: 97, green: 107, blue: 165)
        case .electricity: return UIColor(red: 118, green: 78, blue: 128)
        case .parking: return UIColor(red: 12, green: 71, blue: 238)
        }
    }
    
    static func backgroundColor(for category: String) -> UIColor? {
        return backgroundColor(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func backgroundColor(for type: GraphCategory.CategoryType?) -> UIColor? {
        guard let type = type else { return nil }
        
        switch type {
        case .door: return UIColor(red: 216, green: 199, blue: 197)
        case .furnish: return UIColor(red: 222, green: 193, blue: 186)
        case .windows: return UIColor(red: 225, green: 214, blue: 197)
        case .facade: return UIColor(red: 205, green: 213, blue: 188)
        case .stainedGlass: return UIColor(red: 218, green: 222, blue: 222)
        case .plumbing: return UIColor(red: 203, green: 205, blue: 217)
        case .electricity: return UIColor(red: 182, green: 166, blue: 187)
        case .parking: return UIColor(red: 161, green: 180, blue: 232)
        }
    }
    
    static func accentColor(for category: String) -> UIColor? {
        return accentColor(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func accentColor(for type: GraphCategory.CategoryType?) -> UIColor? {
        guard let type = type else { return nil }
        
        switch type {
        case .door: return UIColor(red: 147, green: 35, blue: 26)
        case .furnish: return UIColor(red: 204, green: 95, blue: 68)
        case .windows: return UIColor(red: 179, green: 137, blue: 75)
        case .facade: return UIColor(red: 128, green: 151, blue: 79)
        case .stainedGlass: return UIColor(red: 145, green: 171, blue: 175)
        case .plumbing: return UIColor(red: 97, green: 107, blue: 165)
        case .electricity: return UIColor(red: 118, green: 78, blue: 128)
        case .parking: return UIColor(red: 12, green: 71, blue: 238)
        }
    }
    
    static func icon(for category: String) -> UIImage? {
        return icon(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func icon(for type: GraphCategory.CategoryType?) -> UIImage? {
        guard let type = type else { return #imageLiteral(resourceName: "new_icon") }
        
        switch type {
        case .door: return #imageLiteral(resourceName: "door_icon")
        case .furnish: return #imageLiteral(resourceName: "furnish_icon")
        case .windows: return #imageLiteral(resourceName: "window_icon")
        case .facade: return #imageLiteral(resourceName: "facade_icon")
        case .stainedGlass: return #imageLiteral(resourceName: "stained_glass_icon")
        case .plumbing: return #imageLiteral(resourceName: "plumbing_icon")
        case .electricity: return #imageLiteral(resourceName: "electricity_icon")
        case .parking: return #imageLiteral(resourceName: "parking_icon")
        }
    }
    
    static func templateIcon(for category: String) -> UIImage {
        return templateIcon(for: GraphCategory.CategoryType(rawValue: category))
    }
    
    static func templateIcon(for type: GraphCategory.CategoryType?) -> UIImage {
        guard let type = type else { return #imageLiteral(resourceName: "new_rounded_template_icon") }
        
        switch type {
        case .door: return #imageLiteral(resourceName: "door_rounded_template_icon")
        case .furnish: return #imageLiteral(resourceName: "furnish_template_icon")
        case .windows: return #imageLiteral(resourceName: "window_rounded_template_icon")
        case .facade: return #imageLiteral(resourceName: "facade_rounded_template_icon")
        case .stainedGlass: return #imageLiteral(resourceName: "stained_glass_rounded_template_icon")
        case .plumbing: return #imageLiteral(resourceName: "plumbing_rounded_template_icon")
        case .electricity: return #imageLiteral(resourceName: "electricity_rounded_template_icon")
        case .parking: return #imageLiteral(resourceName: "parking_rounded_template_icon")
        }
    }
    
    static func qualityColor(for progress: Int) -> UIColor {
        let progress = min(max(0, progress), 100)
        switch progress {
        case 90 ... 100: return UIColor(red: 24, green: 185, blue: 72)
        case 80 ... 89: return UIColor(red: 205, green: 218, blue: 11)
        case 70 ... 79: return UIColor(red: 211, green: 111, blue: 33)
        default: return UIColor(red: 212, green: 51, blue: 40)
        }
    }
    
    static func icon(for type: InfoType) -> UIImage {
        return type == .help ? #imageLiteral(resourceName: "help_icon") : #imageLiteral(resourceName: "literature_icon")
    }
    
}
