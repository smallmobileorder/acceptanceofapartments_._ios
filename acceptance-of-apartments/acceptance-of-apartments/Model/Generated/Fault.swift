//
//  Fault.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 25/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

struct GeneratedFaultSection: Codable {
    let group: String
    let listRemark: [GeneratedFaultDescription]
}

struct GeneratedFaultDescription: Codable {
    let remark: String
    let comment: String
    let currentSubRemark: GeneratedInfo
    let mapPlaceRemark: [String: [GeneratedInfo]]
    let docs: [String]
}

struct GeneratedInfo: Codable {
    let actionIfExist: String
    let levelCritic: Int
    let remark: String
}
