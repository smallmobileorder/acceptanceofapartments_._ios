//
//  InspectionSettings.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 09/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

enum ObjectType {
    case apartment(numberOfRooms: Int, combinedBathroom: Bool, larder: Bool, balcony: Bool, minutes: Int?)
    case placement(numberOfPlacements: Int, minutes: Int?)
    case parking
}

enum FurnishType: Int {
    case full
    case preparing
    case none
}

struct InspectionSettings {
    let object: ObjectType
    let furnishType: FurnishType
}
