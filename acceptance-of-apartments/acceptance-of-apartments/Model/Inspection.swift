//
//  Inspection.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 11/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation
import RealmSwift

class Inspection: Object {
    
    @objc dynamic var uuid = UUID().uuidString
    
    @objc dynamic var userInfo: InspectionUserInfo?
    @objc dynamic var date = Date()
    @objc dynamic var repeatDate: Date?
    @objc private dynamic var storedSettings: StoredInspectionSettings?
    let faults = List<Fault>()
    @objc dynamic var isStarted = false
    @objc dynamic var isMoreThanTwice = false
    @objc dynamic var successor: Inspection?
    
    var settings: InspectionSettings? {
        get {
            guard let storedSettings = storedSettings else { return nil }
            var objectType: ObjectType
            let minutes = storedSettings.minutes.value
            if let numberOfRooms = storedSettings.numberOfRooms.value,
                let combinedBathroom = storedSettings.combinedBathroom.value,
                let larder = storedSettings.larder.value,
                let balcony = storedSettings.balcony.value {
                objectType = .apartment(numberOfRooms: numberOfRooms, combinedBathroom: combinedBathroom, larder: larder, balcony: balcony, minutes: minutes)
            } else if let numberOfPlacements = storedSettings.numberOfPlacements.value {
                objectType = .placement(numberOfPlacements: numberOfPlacements, minutes: minutes)
            } else {
                objectType = .parking
            }
            let furnish = storedSettings.furnishType
            return InspectionSettings(object: objectType, furnishType: FurnishType(rawValue: furnish)!)
        }
        set {
            let storedSettings = StoredInspectionSettings()
            
            guard let settings = newValue else { return }
            
            switch settings.object {
            case .apartment(let numberOfRooms, let combinedBathroom, let larder, let balcony, let minutes):
                storedSettings.numberOfRooms.value = numberOfRooms
                storedSettings.combinedBathroom.value = combinedBathroom
                storedSettings.larder.value = larder
                storedSettings.balcony.value = balcony
                storedSettings.minutes.value = minutes
                
            case .placement(let numberOfPlacements, let minutes):
                storedSettings.numberOfPlacements.value = numberOfPlacements
                storedSettings.minutes.value = minutes
                
            case .parking:
                break
            }
            storedSettings.furnishType = settings.furnishType.rawValue
            
            self.storedSettings = storedSettings
        }
    }
    
    func makeCopy() -> Inspection {
        let inspection = Inspection()
        inspection.userInfo = userInfo
        inspection.date = date
        inspection.repeatDate = repeatDate
        inspection.settings = settings
        for fault in faults {
            let faultCopy = Fault()
            faultCopy.category = fault.category
            faultCopy.fault = fault.fault
            faultCopy.fixed = fault.fixed
            faultCopy.custom = fault.custom
            faultCopy.checked = fault.checked

            faultCopy.details.append(objectsIn: fault.details)
            faultCopy.placement = fault.placement
            faultCopy.comment = fault.comment
            faultCopy.photo = fault.photo
            faultCopy.commentDescription = fault.commentDescription
            faultCopy.actionDescription = fault.actionDescription
            faultCopy.weight = fault.weight

            inspection.faults.append(faultCopy)
        }

        return inspection
    }
    
}

class Fault: Object {
    
    @objc dynamic var category = ""
    @objc dynamic var fault = ""
    @objc dynamic var checked = false
    @objc dynamic var fixed = false
    @objc dynamic var custom = false
    
    let details = List<String>()
    @objc dynamic var placement: String? = nil
    @objc dynamic var comment: String? = nil
    @objc dynamic var photo: String? = nil
    @objc dynamic var commentDescription: String? = nil
    @objc dynamic var actionDescription: String? = nil
    @objc dynamic var weight: Int = 0
    
    override var description: String {
        var description = fault
        var stringDetails: [String] = []
        for detail in details where !detail.isEmpty {
            stringDetails.append(detail)
        }
        if let comment = comment, !comment.isEmpty {
            stringDetails += [comment]
        }
        if !stringDetails.isEmpty {
            description += " (\(stringDetails.joined(separator: ", ")))"
        }
        return description
    }
    
}

class StoredInspectionSettings: Object {
    
    let numberOfRooms = RealmOptional<Int>()
    let combinedBathroom = RealmOptional<Bool>()
    let larder = RealmOptional<Bool>()
    let balcony = RealmOptional<Bool>()
    let numberOfPlacements = RealmOptional<Int>()
    let minutes = RealmOptional<Int>()
    @objc dynamic var furnishType = 0
    
}

class InspectionUserInfo: Object {
    
    let fullNames = List<String>()
    @objc dynamic var complexInfo = ""
    @objc dynamic var address = ""
    @objc dynamic var contractNumber = ""
    @objc dynamic var contractDate: Date?
    @objc dynamic var appartmentNumber = ""
    @objc dynamic var phone = ""
    
}
