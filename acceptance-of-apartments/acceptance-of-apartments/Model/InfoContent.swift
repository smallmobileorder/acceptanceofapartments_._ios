//
//  InfoType.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

enum InfoType: String {
    case help = "Помощь"
    case recommendations = "Общие рекомендации"
    case tools = "Что взять на осмотр"
    case inspectionProcess = "Процесс приемки"
    case faultsCriticity = "Критичность замечаний"
    case warrantyPeriod = "Гарантийный срок"
    case glossary = "Глоссарий"
}

struct InfoSection: Codable {
    let section: String?
    let items: [InfoPayload]
}

struct InfoPayload: Codable {
    let text: String?
    let images: InfoImages?
}

struct InfoImages: Codable {
    
    struct Pair: Codable {
        let title: String
        let named: String
        let width: Double
        let height: Double
    }
    
    let spacing: Double?
    let pairs: [Pair]
    
}
