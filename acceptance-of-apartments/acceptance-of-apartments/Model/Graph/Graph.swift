//
//  Graph.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 09/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation
import RealmSwift

class Graph {
    
    let startedDate: Date
    let categories: [GraphCategory]
    let inspection: Inspection
    let placements: Set<String>
    let timeForInspection: Int?
    let totalWeight: Int
    
    init(startedDate: Date = Date(), categories: [GraphCategory], inspection: Inspection, placements: Set<String>, timeForInspection: Int?, totalWeight: Int) {
        self.startedDate = startedDate
        self.categories = categories
        self.inspection = inspection
        self.placements = placements
        self.timeForInspection = timeForInspection
        self.totalWeight = totalWeight
    }
    
    var isFinished: Bool {
        return !categories.contains(where: { !$0.isFinished })
    }
    
    var totalFaults: Int {
        var totalFaults = 0
        for category in categories {
            totalFaults += category.faults
        }
        return totalFaults
    }
    
    var progress: Float {
        var finishedFaults = 0
        var totalFaults = 0
        for category in categories {
            finishedFaults += category.finishedFaults
            totalFaults += category.totalFaults
        }
        
        return Float(finishedFaults) / Float(totalFaults)
    }
    
    func addUserInfo(names: [String], complex: String, address: String, contractNumber: String, contractDate: Date, appartmentNumber: String, phoneNumber: String) {
        let realm = try! Realm()
        
        if let oldUserInfo = inspection.userInfo {
            try! realm.write {
                realm.delete(oldUserInfo)
                inspection.userInfo = nil
            }
        }
        
        let userInfo = InspectionUserInfo()
        userInfo.fullNames.append(objectsIn: names)
        userInfo.complexInfo = complex
        userInfo.address = address
        userInfo.contractNumber = contractNumber
        userInfo.contractDate = contractDate
        userInfo.appartmentNumber = appartmentNumber
        userInfo.phone = phoneNumber
        
        try! realm.write {
            realm.add(userInfo)
            inspection.userInfo = userInfo
        }
    }
    
    func startInspection() {
        let realm = try! Realm()
        try! realm.write {
            inspection.isStarted = true
        }
    }
    
    func finishInspection() {
        let realm = try! Realm()
        try! realm.write {
            inspection.isStarted = false
        }
    }
    
    func delete(faults: [Fault]) {
        faults.forEach { delete(fault: $0) }
    }
    
    func delete(fault: Fault) {
        let realm = try! Realm()
        try! realm.write {
            if let photo = fault.photo {
                PhotoManager.delete(name: photo)
            }
            realm.delete(fault)
        }
    }
    
    func markAsFixed(faults: [Fault]) {
        let realm = try! Realm()
        for fault in faults {
            try! realm.write {
                fault.fixed = true
            }
        }
    }
    
    func uncheckFault(category: GraphCategory?, sectionName: String?, faultName: String) {
        if let fault = inspection.faults.first(where: { $0.fault == faultName && $0.category == category?.type.rawValue && $0.checked }) {
            delete(fault: fault)
        }
    }
    
    func checkFault(category: GraphCategory?, sectionName: String?, faultName: String) {
        let fault = addFault(category: category, sectionName: sectionName, faultName: faultName, details: [])
        let realm = try! Realm()
        try! realm.write {
            fault.checked = true
        }
    }
    
    func addFault(category: GraphCategory?, sectionName: String?, faultName: String, details: [FaultDetail]) -> Fault {
        if let fault = inspection.faults.first(where: { $0.fault == faultName && $0.category == category?.type.rawValue }) {
            delete(fault: fault)
        }
        let realm = try! Realm()
        
        let isCustom = sectionName == nil
        
        let fault = Fault()
        fault.custom = isCustom
        fault.category = category?.type.rawValue ?? "Новое"
        fault.fault = faultName
        
        for detail in details {
            switch detail {
            case .detail(let value): fault.details.append(value)
            case .placement(let value): fault.placement = value
            case .comment(let value): fault.comment = value
            case .photo(let photo): fault.photo = photo
            case .commentDescription(let value): fault.commentDescription = value
            case .action(let action): fault.actionDescription = action
            case .weight(let value): fault.weight = value
            }
        }
        
        try! realm.write {
            realm.add(fault)
            inspection.faults.append(fault)
        }
        
        if let graphFault = category?.allSections.first(where: { $0.name == sectionName })?.faults.first(where: { $0.name == faultName }) {
            graphFault.state = .fault
        }
        
        return fault
    }
    
}

class GraphCategory {
    
    enum CategoryType: String {
        case door = "Входная дверь"
        case furnish = "Отделка"
        case windows = "Окна"
        case facade = "Фасад"
        case stainedGlass = "Витражи"
        case plumbing = "Сантехника"
        case electricity = "Электрика"
        case parking = "Парковочное место"
    }
    
    let type: CategoryType
    let faultSections: [GraphFaultSection]
    let inspection: Inspection
    
    init(type: CategoryType, faultSections: [GraphFaultSection], inspection: Inspection) {
        self.type = type
        self.faultSections = faultSections
        self.inspection = inspection
        
        for section in faultSections {
            for fault in section.faults {
                fault.category = self
            }
        }
    }
    
    private(set) var customSection = GraphFaultSection(name: nil, faults: [])
    
    var allSections: [GraphFaultSection] {
        return faultSections + [customSection]
    }
    
    var isFinished: Bool {
        for section in faultSections {
            if section.faults.contains(where: { $0.state == nil }) {
                return false
            }
        }
        
        return true
    }
    
    var finishedFaults: Int {
        var finishedFaults = 0
        for section in allSections {
            finishedFaults += section.faults.filter { $0.state != nil }.count
        }
        return finishedFaults
    }
    
    var faults: Int {
        return inspection.faults.filter { !$0.checked && $0.category == self.type.rawValue }.count
    }
    
    var totalFaults: Int {
        var totalFaults = 0
        for section in allSections {
            totalFaults += section.faults.count
        }
        return totalFaults
    }
    
}

class GraphFaultSection {
    
    let name: String?
    var faults: [GraphFault]
    
    init(name: String?, faults: [GraphFault]) {
        self.name = name
        self.faults = faults
    }
    
}

class GraphFault {
    
    let name: String
    let details: [GraphDetail]
    let inspection: Inspection
    let comment: String
    let currentSubRemark: GeneratedInfo
    let mapPlaceRemark: [String: [GeneratedInfo]]
    let docs: [String]
    
    init(name: String, details: [GraphDetail], inspection: Inspection, comment: String, currentSubRemark: GeneratedInfo, mapPlaceRemark: [String: [GeneratedInfo]], docs: [String]) {
        self.name = name
        self.details = details
        self.inspection = inspection
        self.comment = comment
        self.currentSubRemark = currentSubRemark
        self.mapPlaceRemark = mapPlaceRemark
        self.docs = docs
    }
    
    weak var category: GraphCategory!
    
    private(set) lazy var attributedComment: NSAttributedString = {
        var comment = self.comment
        let docs = self.docs
            .map { $0.trimmingCharacters(in: [" "]) }
            .filter { !$0.isEmpty }
            .joined(separator: ", ")
        
        if !docs.isEmpty {
            comment += " (\(docs))"
        }
        
        let attributedString = try! NSAttributedString(data: comment.data(using: .unicode)!,
                                                              options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
        return attributedString
    }()
    
    enum State {
        case ok
        case fault
    }
    
    private var inspectionState: State?
    
    var state: State? {
        get {
            if let inspectionState = inspectionState { return inspectionState }
            
            let fault = inspection.faults.first(where: { $0.fault == self.name && $0.category == self.category.type.rawValue })
            if let fault = fault, fault.checked {
                return .ok
            }
            let containsFault = fault != nil
            switch (inspection.repeatDate == nil, containsFault) {
            case (_, true): return .fault
            case (true, false): return nil
            case (false, false): return .ok
            }
        }
        set {
            inspectionState = newValue
        }
    }
    
}

enum FaultDetail {
    case detail(String)
    case placement(String?)
    case comment(String?)
    case photo(String?)
    case commentDescription(String?)
    case action(String?)
    case weight(Int)
}

enum GraphDetail {
    case details(title: String, options: [String])
    case placements(title: String, options: [String])
}
