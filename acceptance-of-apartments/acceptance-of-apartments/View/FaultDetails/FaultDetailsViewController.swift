//
//  FaultDetailsViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 15/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import RealmSwift
import Lightbox

protocol FaultDetailsViewControllerDelegate: class {
    func didSave(fault: Fault)
}

class FaultDetailsViewController: UIViewController {
    
    weak var delegate: FaultDetailsViewControllerDelegate?
    
    var section: String?
    var graph: Graph!
    var category: GraphCategory?
    var fault: GraphFault?
    
    @IBOutlet private weak var customSectionStackView: UIStackView!
    @IBOutlet private weak var customSectionLabel: UILabel!
    @IBOutlet private weak var sectionNameTextView: TextViewWithPlaceholder?
    @IBOutlet private weak var sectionNameTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var headerView: BrandHeaderView!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var commentLabel: UILabel!
    @IBOutlet private weak var commentTextView: TextViewWithPlaceholder!
    @IBOutlet private weak var commentTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var addPhotoContainer: UIView!
    @IBOutlet private weak var addPhotoLabel: UILabel!
    @IBOutlet private weak var addPhotoImageView: UIImageView!
    @IBOutlet private weak var photoAddedImageView: UIImageView!
    @IBOutlet private weak var saveButton: UIButton!
    
    private lazy var backgroundColor: UIColor? = Style.backgroundColor(for: category?.type)
    private lazy var optionsBackgroundColor: UIColor? = Style.tintColor(for: category?.type)
    private lazy var tintColor: UIColor = Style.textColor(for: category?.type) ?? UIColor(red: 125, green: 114, blue: 114)
    private lazy var accentColor: UIColor = Style.textColor(for: category?.type) ?? UIColor(red: 125, green: 114, blue: 114)
    
    private var checkedRadioGroups: [RadioGroup] = []
    private var checkedPlacementRadioGroup: RadioGroup?
    private var comment: String?
    private var photo: UIImage? {
        didSet {
            photoAddedImageView.isHidden = photo == nil
            addPhotoLabel.text = photo == nil ? "Добавить\nфото" : "Фото\nдобавлено"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        setupUI()
        updateHeight(textView: sectionNameTextView)
        updateHeight(textView: commentTextView)
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        if let photo = photo {
            LightboxConfig.CloseButton.enabled = false
            LightboxConfig.DeleteButton.enabled = true
            LightboxConfig.DeleteButton.text = "Удалить"
            LightboxConfig.DeleteButton.textAttributes = [.font: UIFont.centuryGothic(withSize: 14.0),
                                                          .foregroundColor: UIColor.white]
            LightboxConfig.PageIndicator.enabled = false
            let lightboxVC = LightboxController(images: [LightboxImage(image: photo)], startIndex: 0)
            lightboxVC.dismissalDelegate = self
            present(lightboxVC, animated: true)
        } else {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            let alert = UIAlertController(title: nil, message: "Выберите источник", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Галерея",
                                          style: .default,
                                          handler: { _ in
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Камера",
                                          style: .default,
                                          handler: { _ in
                                            imagePicker.sourceType = .camera
                                            imagePicker.cameraCaptureMode = .photo
                                            self.present(imagePicker, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel))
            
            let popoverPresentationController = alert.popoverPresentationController
            popoverPresentationController?.permittedArrowDirections = []
            popoverPresentationController?.sourceView = view
            popoverPresentationController?.sourceRect = CGRect(x: view.frame.midX, y: view.frame.midY, width: 0.0, height: 0.0)
            
            present(alert, animated: true)
        }
    }
    
    @IBAction func save(_ sender: Any) {
        var success = false
        defer {
            if !success {
                showAlert(title: "Ошибка", message: "Не заполнены обязательные поля")
            }
        }
        for radioGroup in checkedRadioGroups {
            guard radioGroup.selectedButton != nil else { return }
        }
        if let radioGroup = checkedPlacementRadioGroup {
            guard radioGroup.selectedButton != nil else { return }
        }
        
        let faultName = fault?.name ?? sectionNameTextView!.text.trimmingCharacters(in: [" ", "\n"])
        guard !faultName.isEmpty else { return }
        
        var details: [FaultDetail] = []
        var checkedOptions: [String] = []
        for radioGroup in checkedRadioGroups {
            let checkedOption = radioGroup.selectedButton!.name
            checkedOptions.append(checkedOption)
            details.append(.detail(checkedOption))
        }
        details.append(.placement(checkedPlacementRadioGroup?.selectedButton!.name))
        details.append(.comment(commentTextView.text.trimmingCharacters(in: [" ", "\n"])))
        details.append(.photo(PhotoManager.save(photo: photo)))
        if let fault = fault {
            details.append(.commentDescription(fault.comment))
            var groups = fault.mapPlaceRemark["Возможные варианты"] ?? []
            if groups.isEmpty {
                groups = fault.mapPlaceRemark["Место"] ?? []
            }
            if groups.isEmpty {
                groups = fault.mapPlaceRemark["Часть оконного блока"] ?? []
            }
            if groups.isEmpty {
                groups = fault.mapPlaceRemark["Помещение"] ?? []
            }
            let group = groups.first(where: { checkedOptions.contains($0.remark) }) ?? groups.first ?? fault.currentSubRemark
            details.append(.action(group.actionIfExist))
            details.append(.weight(group.levelCritic))
            print(group.levelCritic)
        }
        let newFault = graph.addFault(category: category, sectionName: section, faultName: faultName, details: details)
        
        delegate?.didSave(fault: newFault)
        
        success = true
        
        if navigationController?.popViewController(animated: true) == nil {
            dismiss(animated: true)
        }
    }
    
}

extension FaultDetailsViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        updateHeight(textView: textView)
    }
    
}

private extension FaultDetailsViewController {
    
    func updateHeight(textView: UITextView?) {
        textView?.isScrollEnabled = true
        if textView === commentTextView {
            commentTextViewHeightConstraint.constant = max(70.0, commentTextView.contentSize.height)
        } else if textView === sectionNameTextView {
            sectionNameTextViewHeightConstraint.constant = max(70.0, sectionNameTextView!.contentSize.height)
        }
        textView?.isScrollEnabled = false
    }
    
    func setupUI() {
        navigationController?.navigationBar.tintColor = accentColor
        view.backgroundColor = backgroundColor
        
        headerView.tintColor = Style.strokeColor(for: category?.type) ?? tintColor
        headerView.image = Style.icon(for: category?.type)
        
        scrollView.backgroundColor = backgroundColor
        commentLabel.textColor = accentColor
        customSectionLabel.textColor = accentColor
        
        commentTextView.placeholderColor = tintColor.withAlphaComponent(0.5)
        sectionNameTextView?.placeholderColor = tintColor.withAlphaComponent(0.5)
    }
    
    func configure() {
        if section != nil {
            customSectionStackView.removeFromSuperview()
            customSectionLabel.removeFromSuperview()
            for detail in fault!.details {
                configureDetailSection(detail: detail)
            }
        } else {
            var placements: [String]
            
            let object = graph.inspection.settings!.object
            switch object {
            case .apartment:
                switch category?.type {
                case .door?: placements = ["Коридор"]
                case .facade?, .stainedGlass?: placements = ["Балкон"]
                default: placements = Array(graph.placements)
                }
                
            case .placement, .parking:
                placements = Array(graph.placements)
            }
            
            placements.sort(by: { left, right in left.localizedStandardCompare(right) == .orderedAscending })
            
            configureDetailSection(detail: .placements(title: "Выберите помещение", options: placements))
        }
        configureGeneralSections()
    }
    
    func configureDetailSection(detail: GraphDetail) {
        switch detail {
        case .details(let detail):
            addDetailSection(name: detail.title,
                             options: detail.options,
                             isPlacement: false)
            
        case .placements(let placements):
            addDetailSection(name: placements.title,
                             options: placements.options,
                             isPlacement: true)
        }
    }
    
    func addDetailSection(name: String, options: [String], isPlacement: Bool) {
        guard !options.isEmpty else { return }
        
        let sectionLabel = addSectionLabel(name: name)
        
        let optionsStackView = UIStackView()
        optionsStackView.axis = .vertical
        optionsStackView.spacing = 9.0
        optionsStackView.alignment = .fill
        optionsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        var horizontalStackView = UIStackView()
        var radioButtons: [RadioGroup.RadioButton] = []
        for option in options {
            let optionView = OptionView()
            optionView.tintColor = tintColor
            optionView.accentColor = accentColor
            optionView.optionLabel.textColor = tintColor
            optionView.optionLabel.text = option
            optionView.sizeToFit()
            NSLayoutConstraint.activate([
                optionView.heightAnchor.constraint(greaterThanOrEqualToConstant: 35.0)
            ])
            if horizontalStackView.subviews.count == 2 {
                optionsStackView.addArrangedSubview(horizontalStackView)
                horizontalStackView = UIStackView()
            }
            horizontalStackView.axis = .horizontal
            horizontalStackView.distribution = .fillEqually
            horizontalStackView.spacing = 11.0
            horizontalStackView.addArrangedSubview(optionView)
            radioButtons.append(optionView)
        }
        if !horizontalStackView.subviews.isEmpty {
            optionsStackView.addArrangedSubview(horizontalStackView)
        }
        
        let radioGroup = RadioGroup(radioButtons: radioButtons, selectFirst: radioButtons.count == 1)
        if isPlacement {
            checkedPlacementRadioGroup = radioGroup
        } else {
            checkedRadioGroups.append(radioGroup)
        }
        
        let optionsBackgroundView = UIView()
        optionsBackgroundView.backgroundColor = optionsBackgroundColor
        optionsBackgroundView.layer.cornerRadius = 6.0
        optionsBackgroundView.addSubview(optionsStackView)
        NSLayoutConstraint.activate([
            optionsStackView.topAnchor.constraint(equalTo: optionsBackgroundView.topAnchor, constant: 10.0),
            optionsStackView.leadingAnchor.constraint(equalTo: optionsBackgroundView.leadingAnchor, constant: 10.0),
            optionsStackView.trailingAnchor.constraint(equalTo: optionsBackgroundView.trailingAnchor, constant: -10.0),
            optionsStackView.bottomAnchor.constraint(equalTo: optionsBackgroundView.bottomAnchor, constant: -10.0)
            ])
        
        stackView.insertArrangedSubview(optionsBackgroundView, at: stackView.subviews.count - 2)
        
        sectionLabel.isHidden = options.count == 1
        optionsBackgroundView.isHidden = options.count == 1
    }
    
    func addSectionLabel(name: String) -> UILabel {
        let sectionLabel = UILabel()
        sectionLabel.text = name
        sectionLabel.textAlignment = .center
        sectionLabel.textColor = accentColor
        sectionLabel.font = .centuryGothic(withSize: 14.0)
        stackView.insertArrangedSubview(sectionLabel, at: stackView.subviews.count - 2)
        return sectionLabel
    }
    
    func configureGeneralSections() {
        sectionNameTextView?.delegate = self
        commentTextView.delegate = self
        
        addPhotoLabel.textColor = accentColor
        addPhotoImageView.tintColor = accentColor
        saveButton.backgroundColor = accentColor
        
        for view in [sectionNameTextView, commentTextView, addPhotoContainer, saveButton] {
            view?.layer.cornerRadius = 10.0
            view?.layer.borderWidth = 1.0
            view?.layer.borderColor = accentColor.cgColor
        }
    }
    
}

extension FaultDetailsViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        photo = (info[.editedImage] ?? info[.originalImage]) as? UIImage
        picker.dismiss(animated: true)
    }
    
}

extension FaultDetailsViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        if controller.images.isEmpty {
            photo = nil
        }
    }
    
}
