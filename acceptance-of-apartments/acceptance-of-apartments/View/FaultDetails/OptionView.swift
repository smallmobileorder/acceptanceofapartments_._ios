//
//  OptionView.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 17/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class OptionView: UIButton, Selectable {
    
    var setSelected: Bool = false {
        didSet {
            checkBoxButton.marked = setSelected
        }
    }
    
    override var name: String {
        return optionLabel.text ?? ""
    }
    
    weak var optionLabel: UILabel!
    private weak var checkBoxButton: CheckBoxButton!
    
    var accentColor: UIColor? {
        didSet {
            layer.borderColor = accentColor?.cgColor
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: frame.height + 10.0)
    }

}

private extension OptionView {
    
    func setup() {
        let button = CheckBoxButton()
        button.isUserInteractionEnabled = false
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 4.0
        button.layer.masksToBounds = true
        button.layer.borderColor = tintColor.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8.0),
            button.heightAnchor.constraint(equalToConstant: 29.0),
            button.widthAnchor.constraint(equalTo: button.heightAnchor)
        ])
        checkBoxButton = button
        
        let label = UILabel()
        label.font = .centuryGothic(withSize: 15.0)
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4.0),
            label.topAnchor.constraint(equalTo: topAnchor, constant: 4.0),
            label.trailingAnchor.constraint(equalTo: button.leadingAnchor, constant: -4.0),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4.0),
        ])
        optionLabel = label
        
        contentHorizontalAlignment = .left
        
        backgroundColor = .white
        layer.cornerRadius = 10.0
        layer.borderWidth = 1.0
    }
    
}
