//
//  ObjectChooseViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 08/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class ObjectChooseViewController: UIViewController {
    
    @IBOutlet private var startButton: UIBarButtonItem!
    
    @IBOutlet private weak var objectApartmentButton: RadioButton!
    @IBOutlet private weak var objectPlacementButton: RadioButton!
    @IBOutlet private weak var objectParkingButton: RadioButton!
    private var objectRadioGroup: RadioGroup!
    
    @IBOutlet private weak var apartmentSettingsStackView: UIStackView!
    @IBOutlet private var roomsCountButtons: [RadioButton]!
    private var roomsRadioGroup: RadioGroup!
    @IBOutlet private var combinedBathroomButtons: [RadioButton]!
    private var combinedBathroomRadioGroup: RadioGroup!
    @IBOutlet private var larderButtons: [RadioButton]!
    private var larderRadioGroup: RadioGroup!
    @IBOutlet private var balconyButtons: [RadioButton]!
    private var balconyRadioGroup: RadioGroup!
    
    @IBOutlet private weak var placementSettingsStackView: UIStackView!
    @IBOutlet private var placesCountButtons: [RadioButton]!
    private var placesRadioGroup: RadioGroup!
    
    @IBOutlet private weak var generalSettingsStackView: UIStackView!
    @IBOutlet private weak var timeTextField: UITextField!
    @IBOutlet private weak var timeButton: RadioButton!
    private var timeRadioGroup: RadioGroup!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = nil
        
        objectRadioGroup = RadioGroup(radioButtons: [objectApartmentButton!, objectPlacementButton!, objectParkingButton!], selectFirst: false)
        objectRadioGroup.delegate = self
        
        roomsRadioGroup = RadioGroup(radioButtons: roomsCountButtons, selectFirst: true)
        roomsRadioGroup.delegate = self
        
        combinedBathroomRadioGroup = RadioGroup(radioButtons: combinedBathroomButtons, selectFirst: true)
        combinedBathroomRadioGroup.delegate = self
        
        larderRadioGroup = RadioGroup(radioButtons: larderButtons, selectFirst: true)
        larderRadioGroup.delegate = self
        
        balconyRadioGroup = RadioGroup(radioButtons: balconyButtons, selectFirst: true)
        balconyRadioGroup.delegate = self
        
        placesRadioGroup = RadioGroup(radioButtons: placesCountButtons, selectFirst: true)
        placesRadioGroup.delegate = self
        
        timeRadioGroup = RadioGroup(radioButtons: [timeButton], selectFirst: true)
        timeRadioGroup.delegate = self
        
        timeTextField.delegate = self
        timeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        apartmentSettingsStackView.isHidden = true
        placementSettingsStackView.isHidden = true
        generalSettingsStackView.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let furnishVC = segue.destination as? FurnishChooseViewController {
            var time: Int?
            if let timeString = timeTextField.text {
                time = Int(timeString)
            }
            
            var object: ObjectType
            switch objectRadioGroup.selectedButton {
            case objectApartmentButton:
                let numberOfRooms = Int(roomsRadioGroup.selectedButton!.name) ?? 0
                let combinedBathroom = combinedBathroomRadioGroup.selectedButton!.name == "Да"
                let larder = larderRadioGroup.selectedButton!.name == "Да"
                let balcony = balconyRadioGroup.selectedButton!.name == "Да"
                object = .apartment(numberOfRooms: numberOfRooms,
                                    combinedBathroom: combinedBathroom,
                                    larder: larder,
                                    balcony: balcony,
                                    minutes: time)
            case objectPlacementButton:
                let numberOfPlacements = Int(placesRadioGroup.selectedButton!.name)!
                object = .placement(numberOfPlacements: numberOfPlacements,
                                    minutes: time)
            
            default: return
            }
            
            furnishVC.input = object
        } else if let navVC = segue.destination as? UINavigationController,
            let categoryVC = navVC.topViewController as? CategoryViewController {
            let inspection = InspectionFactory().create(objectType: .parking, furnishType: .none)
            
            let graph = GraphFactory(inspection: inspection)?.create()
            categoryVC.graph = graph
            categoryVC.category = graph?.categories.first
            categoryVC.onDone = { [weak graph] in
                guard graph?.isFinished ?? true else { return }
                
                categoryVC.performSegue(withIdentifier: "showInspectionResultSegue", sender: nil)
            }
            
        }
        
        view.endEditing(true)
    }
    
    @IBAction func next(_ sender: Any) {
        switch objectRadioGroup.selectedButton {
        case objectApartmentButton: fallthrough
        case objectPlacementButton: performSegue(withIdentifier: "showLevelSegue", sender: nil)
        case objectParkingButton: performSegue(withIdentifier: "showParkingSegue", sender: nil)
        default: return
        }
    }
    
}

extension ObjectChooseViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        timeRadioGroup.select(activeButton: nil)
        textField.text = String(textField.text?.prefix(3) ?? "")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            timeRadioGroup.select(activeButton: timeButton)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}

extension ObjectChooseViewController: RadioGroupDelegate {
    
    func radioGroup(_ radioGroup: RadioGroup, didSelectRadioButton radioButton: UIButton?) {
        navigationItem.rightBarButtonItem = startButton
        if radioGroup === objectRadioGroup {
            generalSettingsStackView.isHidden = false
            if radioButton === objectApartmentButton {
                apartmentSettingsStackView.isHidden = false
                placementSettingsStackView.isHidden = true
            } else if radioButton === objectPlacementButton {
                apartmentSettingsStackView.isHidden = true
                placementSettingsStackView.isHidden = false
            } else if radioButton === objectParkingButton {
                apartmentSettingsStackView.isHidden = true
                placementSettingsStackView.isHidden = true
                generalSettingsStackView.isHidden = true
            }
        } else if radioGroup === timeRadioGroup, radioGroup.selectedButton != nil {
            timeTextField.resignFirstResponder()
            timeTextField.text = ""
        }
    }
    
}
