//
//  FurnishChooseViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 12/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class FurnishChooseViewController: UIViewController {
    
    var input: ObjectType!
    
    @IBOutlet private weak var furnishStackView: UIStackView!
    @IBOutlet private weak var furnishButton: UIButton!
    @IBOutlet private weak var preparingButton: UIButton!
    @IBOutlet private weak var noneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if case .placement(_, _)? = input {
            furnishStackView.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navVC = segue.destination as? UINavigationController,
        let categoryListVC = navVC.topViewController as? CategoryListViewController {
            let furnishType = sender as! FurnishType
            let inspection = InspectionFactory().create(objectType: input, furnishType: furnishType)
            let graph = GraphFactory(inspection: inspection)?.create()
            categoryListVC.graph = graph
        }
    }
    
    @IBAction func chooseFurnish(_ sender: Any) {
        performSegue(withIdentifier: "showCategoriesSegue", sender: FurnishType.full)
    }
    
    @IBAction func choosePreparing(_ sender: Any) {
        performSegue(withIdentifier: "showCategoriesSegue", sender: FurnishType.preparing)
    }
    
    @IBAction func chooseNone(_ sender: Any) {
        performSegue(withIdentifier: "showCategoriesSegue", sender: FurnishType.none)
    }
    
    
    

}
