//
//  SplashViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 09/06/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5, execute: { self.performSegue(withIdentifier: "showMainSegue", sender: nil) })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
