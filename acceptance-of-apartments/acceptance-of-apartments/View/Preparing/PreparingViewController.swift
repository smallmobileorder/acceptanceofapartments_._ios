//
//  PreparingViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class PreparingViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let infoVC = segue.destination as? InfoViewController {
            infoVC.type = InfoType(rawValue: (sender as! UIButton).title(for: .normal)!)
        }
    }

}
