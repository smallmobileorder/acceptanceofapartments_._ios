//
//  ResultViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 23/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import XlsxReaderWriter
import RealmSwift

class ResultViewController: UIViewController {
    
    var graph: Graph!
    var faults: [Fault] = []
    var excludedFaults: [Fault] = []
    var isSecond = false
    
    private var sections: [(placement: String, faults: [Fault])] = []
    
    @IBOutlet private weak var finishButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var firstInspectionHeaderView: UIView!
    @IBOutlet private weak var secondInspectionHeaderView: UIView!
    @IBOutlet private weak var repeatInspectionDateLabel: UILabel!
    @IBOutlet private weak var fixedPercentContainer: RoundedView!
    @IBOutlet private weak var fixedFaultsPercentLabel: UILabel!
    @IBOutlet private weak var repeatDescprionLabel: UILabel!
    @IBOutlet private weak var notFixedLabel: UILabel!
    
    private let identifier = String(describing: ResultFaultInfoTableViewCell.self)
    
    @IBOutlet weak var generateButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        graph.finishInspection()
        
        finishButton.isHidden = navigationController?.viewControllers.first != self
        
        if isSecond {
            graph.markAsFixed(faults: excludedFaults)
        } else {
            graph.delete(faults: excludedFaults)
        }
        faults = graph.inspection.faults.array.filter { !$0.checked }
        faults.sort(by: { left, right in
            return (left.placement ?? "") + (left.category) < (right.placement ?? "") + (right.category)
        })
        
        sections = {
            let notFixedFaults = faults.filter { !$0.fixed }
            let placements = Array(Set(notFixedFaults.map { $0.placement ?? "" })).sorted(by: { left, right in
                return left.localizedStandardCompare(right) == .orderedAscending
            })
            return placements.map { placement in
                return (placement: placement, faults: notFixedFaults.filter { $0.placement == placement })
            }
        }()
        
        finishButton.layer.cornerRadius = 8.0
        finishButton.layer.borderWidth = 1.0
        finishButton.layer.borderColor = UIColor(red: 112, green: 112, blue: 112).cgColor
        
        generateButton.layer.cornerRadius = 8.0

        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
        
        firstInspectionHeaderView.isHidden = isSecond
        secondInspectionHeaderView.isHidden = !isSecond
        if isSecond {
            repeatInspectionDateLabel.text = "Повторный осмотр от \(graph.inspection.repeatDate!.formatted("dd.MM.yyyy"))"
            let count = graph.inspection.faults.filter { !$0.checked }.count
            let fixedCount = graph.inspection.faults.filter { !$0.checked && $0.fixed }.count
            var progress = 100
            if count != 0 {
                progress = Int(Double(fixedCount) / Double(count) * 100.0)
            }
            fixedPercentContainer.layer.cornerRadius = 10.0
            fixedPercentContainer.layer.borderWidth = 1.0
            fixedPercentContainer.layer.borderColor = UIColor(red: 125, green: 114, blue: 114).cgColor
            fixedFaultsPercentLabel.text = "\(progress)"
            fixedFaultsPercentLabel.textColor = Style.qualityColor(for: progress)
            
            var description = "Замечания по "
            switch graph.inspection.settings!.object {
            case .apartment: description += "квартире"
            case .placement: description += "помещению"
            case .parking: description += "парковочному месту"
            }
            if let userInfo = graph.inspection.userInfo {
                description += " №\(userInfo.appartmentNumber)"
            }
            description += ", выявленные на осмотре от \(graph.inspection.date.formatted("dd.MM.yyyy")) устранены"
            if count != fixedCount {
                description += " частично"
            }
            description += "."
            repeatDescprionLabel.text = description
            if count == fixedCount {
                notFixedLabel.removeFromSuperview()
            } else {
                let attributtedString = NSMutableAttributedString(string: "Не устранено:".trimmingCharacters(in: ["\n"]), attributes: [.font: UIFont.centuryGothic(withSize: 18.0).withTraits(traits: .traitBold), .foregroundColor: UIColor(red: 125, green: 114, blue: 114)])
                attributtedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: 13))
                notFixedLabel.attributedText = attributtedString
            }
            tableView.tableHeaderView?.frame.size.height = secondInspectionHeaderView.frame.height
        } else {
            tableView.tableHeaderView?.frame.size.height = firstInspectionHeaderView.frame.height
        }
    }
    
    @IBAction func generate(_ sender: Any) {
        let documentPath = Bundle.main.path(forResource: isSecond ? "second_template" : "first_template", ofType: "xlsx")!
        
        let numberRow: ((Int) -> (String)) = { row in return "A\(row)" }
        let placementRow: ((Int) -> (String)) = { row in return "B\(row)" }
        let categoryRow: ((Int) -> (String)) = { row in return "C\(row)" }
        let faultRow: ((Int) -> (String)) = { row in return "D\(row)" }
        let fixedRow: ((Int) -> (String))? = isSecond ? { row in return "E\(row)" } : nil
        
        let spreadsheet = BRAOfficeDocumentPackage.open(documentPath)!
        let worksheet = spreadsheet.workbook.worksheetNamed("Лист1")!

        var id = 1
        var row = isSecond ? 10 : 8
        var previousPlacement: String? = nil
        for fault in faults {
            if (fault.placement ?? "") != previousPlacement {
                worksheet.cell(forCellReference: numberRow(row), shouldCreate: true)?.setIntegerValue(id)
                id += 1
            }
            
            worksheet.cell(forCellReference: placementRow(row), shouldCreate: true)?.setStringValue(fault.placement)
            
            worksheet.cell(forCellReference: categoryRow(row), shouldCreate: true)?.setStringValue(fault.category)
            
            worksheet.cell(forCellReference: faultRow(row), shouldCreate: true)?.setStringValue(fault.description)
            
            if let fixed = fixedRow {
                worksheet.cell(forCellReference: fixed(row), shouldCreate: true)?.setStringValue(fault.fixed ? "Да" : "Нет")
            }
            
            previousPlacement = fault.placement
            row += 1
        }
        
        row += 1
        
        worksheet
            .cell(forCellReference: "B\(row)", shouldCreate: true)?
            .setAttributedStringValue(NSAttributedString(string: "Дольщик(и):",
                                                         attributes: [.font: UIFont.systemFont(ofSize: 11.0).withTraits(traits: .traitBold)]))
        worksheet
            .cell(forCellReference: "D\(row)", shouldCreate: true)?
            .setAttributedStringValue(NSAttributedString(string: "Застройщик:",
                                                         attributes: [.font: UIFont.systemFont(ofSize: 11.0).withTraits(traits: .traitBold)]))
        worksheet
            .cell(forCellReference: "D\(row + 1)", shouldCreate: true)?
            .setStringValue("ФИО:")
        worksheet
            .cell(forCellReference: "D\(row + 2)", shouldCreate: true)?
            .setStringValue("Подпись:")
        worksheet
            .cell(forCellReference: "D\(row + 3)", shouldCreate: true)?
            .setStringValue("Дата:")
        
        let inspection = graph.inspection
        
        var object: String
        switch inspection.settings!.object {
        case .apartment: object = "квартиры"
        case .placement: object = "помещения"
        case .parking: object = "парковочного места"
        }
        worksheet
            .cell(forCellReference: "A1", shouldCreate: true)?
            .setStringValue("Приложение к Акту осмотра \(object) № \(inspection.userInfo?.appartmentNumber ?? "_______") от \(inspection.date.formatted("dd.MM.yyyy"))")
        
        
        if isSecond {
            worksheet
                .cell(forCellReference: "A4", shouldCreate: true)?
                .setStringValue("Повторный осмотр от \(inspection.repeatDate?.formatted("dd.MM.yyyy") ?? "")")
        }
        
        if let userInfo = graph.inspection.userInfo {
            worksheet
                .cell(forCellReference: "A2", shouldCreate: true)?
                .setStringValue("по Договору №\(userInfo.contractNumber) от \(userInfo.contractDate?.formatted("dd.MM.yyyy") ?? "_______")")
            
            worksheet
                .cell(forCellReference: "A\(isSecond ? 5 : 3)", shouldCreate: true)?
                .setStringValue("Дольщик(и): \(userInfo.fullNames.array.joined(separator: ", "))")
            
            worksheet
                .cell(forCellReference: "A\(isSecond ? 6 : 4)", shouldCreate: true)?
                .setStringValue("Телефон для связи: \(userInfo.phone)")
            
            for user in userInfo.fullNames {
                row += 1
                worksheet
                    .cell(forCellReference: "B\(row)", shouldCreate: true)?
                    .setStringValue("ФИО:")
                worksheet
                    .cell(forCellReference: "C\(row)", shouldCreate: true)?
                    .setStringValue(user)
                
                row += 1
                worksheet
                    .cell(forCellReference: "B\(row)", shouldCreate: true)?
                    .setStringValue("Подпись:")
                
                row += 1
                worksheet
                    .cell(forCellReference: "B\(row)", shouldCreate: true)?
                    .setStringValue("Дата:")
            }
        }
        
        if isSecond {
            let allFixed = !inspection.faults.contains(where: { !$0.checked && !$0.fixed })
            
            worksheet
                .cell(forCellReference: "A8", shouldCreate: true)?
                .setStringValue("При осмотре \(object) №\(inspection.userInfo?.appartmentNumber ?? "______"), выявленные на осмотре от \(inspection.date.formatted("dd.MM.yyyy")) устранены \(allFixed ? "в полном объеме." : "частично.")")
            
        } else {
            worksheet
                .cell(forCellReference: "A6", shouldCreate: true)?
                .setStringValue("При осмотре \(object) №\(inspection.userInfo?.appartmentNumber ?? "______") в ЖК \"\(inspection.userInfo?.complexInfo ?? "____________")\", расположенном по адресу: \"\(inspection.userInfo?.address ?? "__________________")\", дольщиком были выявлены следующие недостатки:")
        }
        
        let path = FileManager.default.documentsDirectory!
        let name = "/\(UUID().uuidString).xlsx"
        
        spreadsheet.save(as: path.appending(name))
        
        let activityVC = UIActivityViewController(activityItems: [URL(fileURLWithPath: path.appending(name))], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = generateButton
        activityVC.popoverPresentationController?.permittedArrowDirections = .down
        present(activityVC, animated: true)
    }
    
    @IBAction func close(_ sender: Any) {
        performSegue(withIdentifier: "unwindToMenu", sender: nil)
    }
    
}

extension ResultViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ResultFaultInfoTableViewCell
        
        let section = sections[indexPath.row]
        cell.configure(placement: section.placement, faults: section.faults)
        
        return cell
    }
    
    
}
