//
//  ResultFaultInfoTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 26/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class ResultFaultInfoTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var placementLabel: UILabel!
    @IBOutlet private weak var faultsStackView: UIStackView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        faultsStackView.subviews.forEach { $0.removeFromSuperview() }
    }
    
    func configure(placement: String, faults: [Fault]) {
        placementLabel.text = placement
        
        var previousCategory: String? = nil
        var text = ""
        var previousCategoryLabel: UILabel!
        for fault in faults {
            if fault.category != previousCategory {
                if let previousCategoryLabel = previousCategoryLabel {
                    let attributtedString = NSMutableAttributedString(string: text.trimmingCharacters(in: ["\n"]), attributes: [.font: UIFont.centuryGothic(withSize: 14.0), .foregroundColor: UIColor(red: 125, green: 114, blue: 114)])
                    attributtedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: previousCategory!.count))
                    previousCategoryLabel.attributedText = attributtedString
                    faultsStackView.addArrangedSubview(previousCategoryLabel)
                }
                previousCategoryLabel = UILabel()
                previousCategoryLabel.numberOfLines = 0
                text = "\(fault.category): "
            }
            
            text += fault.description + "\n"
            
            previousCategory = fault.category
        }
        if let previousCategoryLabel = previousCategoryLabel {
            let attributtedString = NSMutableAttributedString(string: text.trimmingCharacters(in: ["\n"]), attributes: [.font: UIFont.centuryGothic(withSize: 14.0), .foregroundColor: UIColor(red: 125, green: 114, blue: 114)])
            attributtedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: previousCategory!.count))
            previousCategoryLabel.attributedText = attributtedString
            faultsStackView.addArrangedSubview(previousCategoryLabel)
        }
    }
    
}
