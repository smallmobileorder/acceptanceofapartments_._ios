//
//  FaultDescriptionTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 22/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class FaultDescriptionTableViewCell: UITableViewCell {
    
    private var faultDetailViewModel: FaultDetailViewModel!
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var categoryImageView: UIImageView!
    @IBOutlet private weak var categoryNameLabel: UILabel!
    @IBOutlet private weak var photoButton: UIButton!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var closedImageView: UIImageView!
    
    @IBOutlet private weak var stateLabel: UILabel!
    @IBOutlet private weak var yesButton: UIButton!
    @IBOutlet private weak var noButton: UIButton!
    
    @IBOutlet private weak var expandView: RoundedView!
    @IBOutlet private weak var splitterView: UIView!
    @IBOutlet private weak var commentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
    }
    
    @IBAction func cancel(_ sender: Any) {
        faultDetailViewModel.state = .canceled
    }
    
    @IBAction func apply(_ sender: Any) {
        faultDetailViewModel.state = .applied
    }
    
    @IBAction func expand(_ sender: Any) {
        faultDetailViewModel.isExpanded.toggle()
    }
    
    @IBAction func showPhoto(_ sender: Any) {
        faultDetailViewModel.showPhoto()
    }
    
    func configure(faultDetailViewModel: FaultDetailViewModel, isSecond: Bool, faded: Bool, available: Bool) {
        self.faultDetailViewModel = faultDetailViewModel
        let fault = faultDetailViewModel.fault
        
        stateLabel.text = isSecond ? "Устранено?" : "Выдать замечание?"
        
        categoryImageView.image = Style.templateIcon(for: fault.category)
        categoryNameLabel.text = fault.category
        
        let hasPhoto = fault.photo != nil
        photoButton.isEnabled = hasPhoto
        photoButton.setImage(hasPhoto ? #imageLiteral(resourceName: "photo_standard_icon") : #imageLiteral(resourceName: "no_photo_icon"), for: .normal)
        
        let font = UIFont.centuryGothic(withSize: 20.0)
        let boldFont = UIFont.centuryGothic(withSize: 20.0).withTraits(traits: .traitBold)
        if let state = faultDetailViewModel.state {
            let applied = state == .applied
            if applied {
                yesButton.titleLabel?.font = boldFont
                noButton.titleLabel?.font = font
            } else {
                yesButton.titleLabel?.font = font
                noButton.titleLabel?.font = boldFont
            }
        } else {
            yesButton.titleLabel?.font = font
            noButton.titleLabel?.font = font
        }
        
        if faded {
            containerView.backgroundColor = UIColor(red: 172, green: 165, blue: 165)
        } else {
            containerView.backgroundColor = Style.strokeColor(for: fault.category) ?? UIColor(red: 125, green: 114, blue: 114)
        }
        
        closedImageView.isHidden = available
        
        descriptionLabel.text = fault.description
        
        if faultDetailViewModel.isExpandable {
            expandView.isHidden = faultDetailViewModel.isExpanded
            splitterView.isHidden = !faultDetailViewModel.isExpanded
            commentLabel.isHidden = !faultDetailViewModel.isExpanded
        } else {
            expandView.isHidden = true
            splitterView.isHidden = true
            commentLabel.isHidden = true
        }
        commentLabel.text = fault.actionDescription
    }
    
}
