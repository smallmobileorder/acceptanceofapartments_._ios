//
//  FaultsListViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 22/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import RealmSwift
import Lightbox

class FaultsListViewController: UIViewController {
    
    var graph: Graph!
    var isSecond = false
    //    private var faults: [Fault] = []
    private var viewModels: [FaultDetailViewModel] = []
    private var filteredViewModels: [FaultDetailViewModel] = []
    private lazy var placements: [String] = {
        var sorted = Array(graph.placements).sorted(by: { left, right in
            return left.localizedStandardCompare(right) == .orderedAscending
        })
        sorted.insert("Все", at: 0)
        return sorted
    }()
    
    @IBOutlet private weak var continueButton: UIBarButtonItem!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var footerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var inspectionInfoLabel: UILabel!
    @IBOutlet private weak var placementSelectTextField: UITextField!
    
    private let identifier = String(describing: FaultDescriptionTableViewCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        updateContinueButton()
        setupViewModels()
        updateTableView()
        updateContinueButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let faultDetailsVC = segue.destination as? FaultDetailsViewController {
            faultDetailsVC.delegate = self
            faultDetailsVC.section = nil
            faultDetailsVC.category = nil
            faultDetailsVC.graph = graph
            faultDetailsVC.fault = nil
        } else {
            var excludedViewModels: [FaultDetailViewModel]
            if isSecond {
                excludedViewModels = viewModels.filter { $0.state == .applied }
            } else {
                excludedViewModels = viewModels.filter { $0.state == .canceled }
            }
            let faults = graph.inspection.faults.array.filter { !$0.checked }
            let excludedFaults = excludedViewModels.map { $0.fault }
            if let navVC = segue.destination as? UINavigationController,
                let resultVC = navVC.viewControllers.first as? ResultViewController {
                resultVC.graph = graph
                resultVC.faults = faults
                resultVC.excludedFaults = excludedFaults
                resultVC.isSecond = isSecond
            } else if let userInfoVC = segue.destination as? UserInfoViewController {
                userInfoVC.graph = graph
                userInfoVC.faults = faults
                userInfoVC.excludedFaults = excludedFaults
                userInfoVC.isSecond = isSecond
            }
        }
    }
    
    
    @IBAction func `continue`(_ sender: Any) {
        if graph.inspection.userInfo == nil && !isSecond {
            performSegue(withIdentifier: "showUserInfoSegue", sender: nil)
        } else {
            performSegue(withIdentifier: "showResultSegue", sender: nil)
        }
    }
    
    @IBAction func addNewFault(_ sender: Any) {
        performSegue(withIdentifier: "showFaultDetailsSegue", sender: nil)
    }
    
}

private extension FaultsListViewController {
    
    func setup() {
        view.setDismissKeyboardOnTap()
        
        placementSelectTextField.text = placements.first
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        placementSelectTextField.inputView = pickerView
        
        if isSecond {
            titleLabel.text = "Повторный осмотр"
            var description = "Замечания по "
            switch graph.inspection.settings!.object {
            case .apartment: description += "квартире"
            case .placement: description += "помещению"
            case .parking: description += "парковочному месту"
            }
            if let userInfo = graph.inspection.userInfo {
                description += " №\(userInfo.appartmentNumber)"
            }
            description += ", выявленные на осмотре от \(graph.inspection.date.formatted("dd.MM.yyyy"))"
            inspectionInfoLabel.text = description
        } else {
            inspectionInfoLabel.isHidden = true
            footerView.frame.size.height = 0.0
        }
        
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
    }
    
    func setupViewModels() {
        for fault in graph.inspection.faults.array where !fault.checked {
            addViewModel(fault: fault)
        }
        
        tableView.reloadData()
    }
    
    func addViewModel(fault: Fault) {
        let viewModel = FaultDetailViewModel(fault: fault, isSecond: isSecond, isMoreThanTwice: graph.inspection.isMoreThanTwice)
        viewModel.onExpand = { [unowned self, viewModel] expanded in
            UIView.performWithoutAnimation {
                self.tableView.reloadRows(at: [viewModel.indexPath], with: .none)
            }
        }
        viewModel.onApply = { [unowned self, viewModel] in
            self.updateContinueButton()
            if self.isSecond {
                let realm = try! Realm()
                try! realm.write {
                    viewModel.fault.fixed = true
                }
            }
            UIView.performWithoutAnimation {
                self.tableView.reloadRows(at: [viewModel.indexPath], with: .none)
            }
        }
        viewModel.onCancel = { [unowned self, viewModel] in
            self.updateContinueButton()
            if self.isSecond {
                let realm = try! Realm()
                try! realm.write {
                    viewModel.fault.fixed = false
                }
            }
            UIView.performWithoutAnimation {
                self.tableView.reloadRows(at: [viewModel.indexPath], with: .none)
            }
        }
        viewModel.onShowPhoto = { [unowned self] photo in
            self.updateContinueButton()
            LightboxConfig.CloseButton.enabled = false
            LightboxConfig.PageIndicator.enabled = false
            LightboxConfig.DeleteButton.enabled = false
            let lightboxVC = LightboxController(images: [LightboxImage(image: PhotoManager.get(name: photo) ?? UIImage())], startIndex: 0)
            self.present(lightboxVC, animated: true)
        }
        
        viewModels.append(viewModel)
    }
    
    func updateTableView() {
        let placement = placementSelectTextField.text ?? ""
        
        if placement == "Все" {
            filteredViewModels = viewModels
        } else {
            filteredViewModels = viewModels.filter { $0.fault.placement == placement }
        }
        
        tableView.reloadData()
    }
    
    func updateContinueButton() {
        continueButton.isEnabled = !viewModels.contains(where: { $0.state == nil })
    }
    
}

extension FaultsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FaultDescriptionTableViewCell
        
        let viewModel = filteredViewModels[indexPath.row]
        viewModel.indexPath = indexPath
        var faded = false
        var available = true
        if let state = viewModel.state {
            let applied = state == .applied
            faded = isSecond ? applied : !applied
            available = applied
        }
        cell.configure(faultDetailViewModel: viewModel,
                       isSecond: isSecond,
                       faded: faded,
                       available: available)
        
        return cell
    }
    
}

extension FaultsListViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return placements.count
    }
    
}

extension FaultsListViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return placements[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        placementSelectTextField.text = placements[row]
        updateTableView()
    }
    
}

extension FaultsListViewController: FaultDetailsViewControllerDelegate {
    
    func didSave(fault: Fault) {
        addViewModel(fault: fault)
        updateTableView()
        updateContinueButton()
    }
    
}
