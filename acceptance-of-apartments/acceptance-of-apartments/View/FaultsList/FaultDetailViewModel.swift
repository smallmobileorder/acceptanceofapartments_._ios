//
//  FaultDetailViewModel.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 23/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

class FaultDetailViewModel {
    
    enum State {
        case applied
        case canceled
    }
    
    let fault: Fault
    private let isSecond: Bool
    var state: State? = nil {
        didSet {
            switch state {
            case .applied?: onApply?()
            case .canceled?: onCancel?()
            default: return
            }
        }
    }
    
    init(fault: Fault, isSecond: Bool, isMoreThanTwice: Bool) {
        self.fault = fault
        self.isSecond = isSecond
        if isSecond {
            if isMoreThanTwice {
                state = fault.fixed ? .applied : .canceled
            }
        } else {
            state = .applied
        }
    }
    
    var indexPath: IndexPath!
    
    var onExpand: ((Bool) -> ())?
    var onApply: (() -> ())?
    var onCancel: (() -> ())?
    var onShowPhoto: ((String) -> ())?
    
    var isExpandable: Bool {
        return fault.actionDescription != nil && !isSecond
    }
    
    var isExpanded = false {
        didSet {
            onExpand?(isExpanded)
        }
    }
    
    func showPhoto() {
        if let photo = fault.photo {
            onShowPhoto?(photo)
        }
    }

}
