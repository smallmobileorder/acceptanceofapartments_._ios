//
//  InspectionResultViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 22/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import RealmSwift

class InspectionResultViewController: UIViewController {
    
    var graph: Graph!
    var isSecond = false
    
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var qualityContainerView: RoundedView!
    @IBOutlet private weak var qualityLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let faultsListVC = segue.destination as? FaultsListViewController {
            faultsListVC.graph = graph
        } else if let resultVC = segue.destination as? ResultViewController {
            resultVC.graph = graph
            resultVC.faults = graph.inspection.faults.array.filter { !$0.checked }
        } else if let userInfoVC = segue.destination as? UserInfoViewController {
            userInfoVC.graph = graph
            userInfoVC.faults = graph.inspection.faults.array.filter { !$0.checked }
            userInfoVC.excludedFaults = []
            userInfoVC.isSecond = isSecond
        }
    }
    
    @IBAction func close(_ sender: Any) {
        performSegue(withIdentifier: "unwindToMenu", sender: nil)
    }
    
    @IBAction func `continue`(_ sender: Any) {
        if isSecond { performSegue(withIdentifier: "showResultSegue", sender: nil) }
        else if graph.inspection.faults.contains(where: { $0.checked && !$0.fixed }) { performSegue(withIdentifier: "showFaultsListSegue", sender: nil) }
        else { performSegue(withIdentifier: "showUserInfoSegue", sender: nil) }
    }
    
}

private extension InspectionResultViewController {
    
    func setup() {
        if navigationController?.viewControllers.first != self {
            navigationItem.leftBarButtonItem = nil
        }
        
        let totalFaults = graph.totalFaults
        for category in graph.categories {
            add(category: category, totalFaults: totalFaults)
        }
        
        let totalWeight = graph.totalWeight
        let realFaults = graph.inspection.faults.array.filter { !$0.checked }
        let weightSum = realFaults.map { $0.weight }.reduce(0, +)
        let criticalCount = realFaults.filter { $0.weight == 10 }.count
        let progress = Int(Double(totalWeight - weightSum) / Double(totalWeight) * 100)
        let finalProgress = max(0, progress - criticalCount * 30)
        
        qualityContainerView.layer.cornerRadius = 10.0
        qualityContainerView.layer.borderWidth = 1.0
        qualityContainerView.layer.borderColor = UIColor(red: 125, green: 114, blue: 114).cgColor
        qualityLabel.text = "\(finalProgress)"
        qualityLabel.textColor = Style.qualityColor(for: finalProgress)
    }
    
    func add(category: GraphCategory, totalFaults: Int) {
        let faults = category.faults
        
        guard faults != 0 else { return }
        
        let color = Style.strokeColor(for: category.type)!
        
        let horizontalStackView = UIStackView()
        horizontalStackView.spacing = 12.0
        
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.text = String(category.type.rawValue.prefix(2))
        titleLabel.textColor = color
        titleLabel.layer.cornerRadius = 10.0
        titleLabel.layer.borderWidth = 1.0
        titleLabel.layer.borderColor = color.cgColor
        NSLayoutConstraint.activate([
            titleLabel.widthAnchor.constraint(equalToConstant: 37.0),
            titleLabel.heightAnchor.constraint(equalTo: titleLabel.widthAnchor)
        ])
        
        horizontalStackView.addArrangedSubview(titleLabel)
        
        let badgeLabel = UILabel()
        badgeLabel.textAlignment = .center
        badgeLabel.text = "\(faults)"
        badgeLabel.textColor = .white
        badgeLabel.backgroundColor = color
        badgeLabel.layer.cornerRadius = 11.5
        badgeLabel.layer.masksToBounds = true
        badgeLabel.translatesAutoresizingMaskIntoConstraints = false
        horizontalStackView.addSubview(badgeLabel)
        NSLayoutConstraint.activate([
            badgeLabel.widthAnchor.constraint(equalToConstant: 23.0),
            badgeLabel.heightAnchor.constraint(equalTo: badgeLabel.widthAnchor),
            badgeLabel.centerXAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            badgeLabel.centerYAnchor.constraint(equalTo: titleLabel.topAnchor),
        ])
        
        let progressContainer = UIView()
        
        let progressView = UIView()
        progressView.backgroundColor = color
        progressView.layer.cornerRadius = 7.0
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressContainer.addSubview(progressView)
        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: progressContainer.leadingAnchor),
            progressView.widthAnchor.constraint(equalTo: progressContainer.widthAnchor, multiplier: CGFloat(faults) / CGFloat(totalFaults)),
            progressView.centerYAnchor.constraint(equalTo: progressContainer.centerYAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 14.0)
        ])
        
        horizontalStackView.addArrangedSubview(progressContainer)
        
        stackView.addArrangedSubview(horizontalStackView)
    }
    
}
