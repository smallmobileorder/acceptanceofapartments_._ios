//
//  FaultTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 13/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class FaultCollectionViewCell: UICollectionViewCell {
    
    var onAdd: (() -> ())?
    var onCheck: (() -> ())?
    var onExpand: ((CGFloat) -> ())?
    
    private weak var addButton: AddButton!
    private weak var titleLabel: UILabel!
    private weak var checkBoxButton: CheckBoxButton!
    private weak var commentLabel: UILabel!
    private weak var separatorView: UIView!
    private weak var expandView: UIView!
    private weak var xMarkImageView: UIImageView!
    
    override var tintColor: UIColor! {
        didSet {
            titleLabel.textColor = tintColor
        }
    }
    
    var smoothColor: UIColor = .clear {
        didSet {
            separatorView.backgroundColor = smoothColor
            expandView.backgroundColor = smoothColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        onAdd = nil
        onCheck = nil
        onExpand = nil
    }
    
    func configure(viewModel: ExpandedFaultViewModel, onAdd: @escaping () -> (), onCheck: @escaping () -> (), onExpand: @escaping (CGFloat) -> ()) {
        let fault = viewModel.fault
        
        commentLabel.isHidden = !viewModel.expanded
        separatorView.isHidden = !viewModel.expanded
        expandView.isHidden = viewModel.expanded
        
        titleLabel.text = fault.name
        addButton.active = false
        checkBoxButton.marked = false
        checkBoxButton.filled = false
        xMarkImageView.isHidden = true
        switch fault.state {
        case .ok?:
            checkBoxButton.marked = true
            let crossedString = NSMutableAttributedString(string: fault.name,
                                                         attributes: [.font: titleLabel.font,
                                                                      .foregroundColor: titleLabel.textColor.withAlphaComponent(0.4)])
            crossedString.addAttribute(.strikethroughStyle, value: 1, range: NSRange(location: 0, length: crossedString.length))
            titleLabel.attributedText = crossedString
        case .fault?:
            xMarkImageView.isHidden = false
            checkBoxButton.filled = true
            addButton.active = true
        case nil: break
        }
        
        let attributedString = NSMutableAttributedString(attributedString: fault.attributedComment)
        
        attributedString.setFontFace(font: .centuryGothic(withSize: 12.0),
                                     color: tintColor)
        
        commentLabel.attributedText = attributedString
        commentLabel.textAlignment = .center
        
        self.onAdd = onAdd
        self.onCheck = onCheck
        self.onExpand = onExpand
    }

}

private extension FaultCollectionViewCell {
    
    func setup() {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        let containerView = UIView()
        containerView.layer.cornerRadius = 4.0
        containerView.backgroundColor = .white
        containerView.frame = bounds
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(stackView)
        addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 3.0),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -3.0)
        ])
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 13.0),
            stackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 12.0),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -13.0),
            stackView.heightAnchor.constraint(equalToConstant: 32.0)
        ])
        
        let addButton = AddButton()
        NSLayoutConstraint.activate([
            addButton.heightAnchor.constraint(equalTo: addButton.widthAnchor)
        ])
        
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.font = UIFont.centuryGothic(withSize: 15.0)
        
        let checkBoxButton = CheckBoxButton()
        checkBoxButton.layer.borderWidth = 1.0
        checkBoxButton.layer.cornerRadius = 4.0
        checkBoxButton.layer.masksToBounds = true
        checkBoxButton.layer.borderColor = tintColor.cgColor
        NSLayoutConstraint.activate([
            checkBoxButton.heightAnchor.constraint(equalTo: checkBoxButton.widthAnchor)
        ])
        
        stackView.addArrangedSubview(addButton)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(checkBoxButton)
        
        self.addButton = addButton
        self.titleLabel = titleLabel
        self.checkBoxButton = checkBoxButton
        
        addButton.addTarget(self, action: #selector(add), for: .touchUpInside)
        checkBoxButton.addTarget(self, action: #selector(check), for: .touchUpInside)
        
        let commentLabel = UILabel()
        commentLabel.textAlignment = .center
        commentLabel.font = .centuryGothic(withSize: 12.0)
        commentLabel.numberOfLines = 0
        commentLabel.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(commentLabel)
        NSLayoutConstraint.activate([
            commentLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 10.0),
            commentLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            commentLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
        self.commentLabel = commentLabel
        
        let expandButton = UIButton()
        expandButton.addTarget(self, action: #selector(expand), for: .touchUpInside)
        expandButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(expandButton)
        NSLayoutConstraint.activate([
            expandButton.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            expandButton.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            expandButton.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            expandButton.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor)
        ])
        
        let separatorView = UIView()
        separatorView.backgroundColor = .red
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorView)
        NSLayoutConstraint.activate([
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            separatorView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 4.0),
            separatorView.leadingAnchor.constraint(equalTo: addButton.trailingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: checkBoxButton.leadingAnchor)
            ])
        self.separatorView = separatorView
        
        let expandView = RoundedView()
        expandView.topLeft = true
        expandView.topRight = true
        expandView.bottomLeft = true
        expandView.bottomRight = true
        expandView.radius = 1.5
        expandView.backgroundColor = .red
        expandView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(expandView)
        NSLayoutConstraint.activate([
            expandView.heightAnchor.constraint(equalToConstant: 3.0),
            expandView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 4.0),
            expandView.centerXAnchor.constraint(equalTo: centerXAnchor),
            expandView.widthAnchor.constraint(equalToConstant: 43.0)
            ])
        self.expandView = expandView
        
        let xMarkImageView = UIImageView(image: #imageLiteral(resourceName: "letter-x"))
        xMarkImageView.tintColor = .white
        xMarkImageView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(xMarkImageView, aboveSubview: checkBoxButton)
        NSLayoutConstraint.activate([
            xMarkImageView.topAnchor.constraint(equalTo: checkBoxButton.topAnchor, constant: 8.0),
            xMarkImageView.leadingAnchor.constraint(equalTo: checkBoxButton.leadingAnchor, constant: 8.0),
            xMarkImageView.trailingAnchor.constraint(equalTo: checkBoxButton.trailingAnchor, constant: -8.0),
            xMarkImageView.bottomAnchor.constraint(equalTo: checkBoxButton.bottomAnchor, constant: -8.0)
        ])
        self.xMarkImageView = xMarkImageView
    }
    
    @objc func add() {
        onAdd?()
    }
    
    @objc func check() {
        onCheck?()
    }
    
    @objc func expand() {
        onExpand?(commentLabel.frame.height + 8.0)
    }
    
}
