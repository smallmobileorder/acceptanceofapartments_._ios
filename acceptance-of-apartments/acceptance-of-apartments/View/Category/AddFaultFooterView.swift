//
//  AddFaultFooterView.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 24/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class AddFaultFooterView: UICollectionReusableView {
    
    @IBOutlet private weak var addLabel: UILabel!
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var buttonWrapperView: RoundedView!
    
    private var onAdd: (() -> ())?
    private var onDone: (() -> ())?
    
    @IBAction func add(_ sender: Any) {
        onAdd?()
    }
    
    @IBAction func done(_ sender: Any) {
        onDone?()
    }
    
    func configure(color: UIColor, onAdd: @escaping () -> (), onDone: @escaping () -> ()) {
        tintColor = color
        addLabel.textColor = color
        backgroundView.backgroundColor = color
        buttonWrapperView.backgroundColor = color
        self.onAdd = onAdd
        self.onDone = onDone
    }
    
}
