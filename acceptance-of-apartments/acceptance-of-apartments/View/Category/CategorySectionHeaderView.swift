//
//  CategorySectionHeaderView.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 20/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class CategorySectionHeaderView: UICollectionReusableView {
    
    private(set) weak var label: UILabel!
    
    override var tintColor: UIColor! {
        didSet {
            label.textColor = tintColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
        
}

private extension CategorySectionHeaderView {
    
    func setup() {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .centuryGothic(withSize: 18.0)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8.0),
            label.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        self.label = label
    }
    
}
