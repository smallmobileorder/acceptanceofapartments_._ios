//
//  CategoryViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 13/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class ExpandedFaultViewModel {
    
    let fault: GraphFault
    
    init(_ fault: GraphFault) {
        self.fault = fault
    }
    
    var expanded = false
    var height: CGFloat = 60.0
    var expandedHeight: CGFloat = 0.0
    
}

class CategoryViewController: UIViewController {
    
    var graph: Graph!
    var category: GraphCategory!
    var onDone: (() -> ())?
    private var isAllChecked = false {
        didSet {
            if isAllChecked {
                selectAllButton.title = "Снять все"
            } else {
                selectAllButton.title = "Отметить все"
            }
        }
    }
    
    @IBOutlet private weak var selectAllButton: UIBarButtonItem!
    @IBOutlet private weak var headerView: BrandHeaderView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private let headerIdentifier = String(describing: CategorySectionHeaderView.self)
    private let marginIdentifier = String(describing: MarginCollectionViewCell.self)
    private let identifier = String(describing: FaultCollectionViewCell.self)
    private let footerIdentifier = String(describing: AddFaultFooterView.self)
    
    private lazy var backgroundColor: UIColor = Style.backgroundColor(for: category.type)!
    private lazy var tintColor: UIColor = Style.tintColor(for: category.type)!
    private lazy var accentColor: UIColor = Style.accentColor(for: category.type)!
    
    private var sections: [String: [ExpandedFaultViewModel]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        graph.startInspection()
        
        setupViewModel()
        styleUI()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? FaultDetailsViewController {
            var section: GraphFaultSection?
            var fault: GraphFault?
            if let indexPath = sender as? IndexPath {
                section = category.faultSections[indexPath.section]
                fault = section?.faults[indexPath.row - 1]
            }
            detailsVC.section = section?.name
            detailsVC.category = category
            detailsVC.graph = graph
            detailsVC.fault = fault
        } else if let navVC = segue.destination as? UINavigationController,
            let resultVC = navVC.viewControllers.first as? InspectionResultViewController {
            resultVC.graph = graph
        }
    }
    
    @IBAction func checkAll(_ sender: Any) {
        isAllChecked.toggle()
        
        for viewModels in sections.values {
            for viewModel in viewModels {
                if isAllChecked {
                    if viewModel.fault.state != .fault {
                        viewModel.fault.state = .ok
                        graph.checkFault(category: category, sectionName: nil, faultName: viewModel.fault.name)
                    }
                } else {
                    viewModel.fault.state = nil
                    graph.uncheckFault(category: category, sectionName: nil, faultName: viewModel.fault.name)
                }
            }
        }
        collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems)
    }
    
}

private extension CategoryViewController {
    
    func setupViewModel() {
        for section in category.faultSections {
            sections[section.name!] = section.faults.map { ExpandedFaultViewModel.init($0) }
        }
        isAllChecked = category.isFinished
    }
    
    func styleUI() {
        navigationController?.navigationBar.tintColor = accentColor
        if navigationController?.viewControllers.first == self {
            let backButton = UIBarButtonItem(title: "Назад", style: .done, target: self, action: #selector(back))
            navigationItem.leftBarButtonItem = backButton
        }
        view.backgroundColor = backgroundColor
        
        headerView.tintColor = Style.strokeColor(for: category.type)
        headerView.image = Style.icon(for: category.type)
        collectionView.backgroundColor = backgroundColor
    }
    
    @objc func back() {
        dismiss(animated: true)
    }
    
    func setupCollectionView() {
        collectionView.register(MarginCollectionViewCell.self, forCellWithReuseIdentifier: marginIdentifier)
        collectionView.register(FaultCollectionViewCell.self, forCellWithReuseIdentifier: identifier)
        collectionView.register(CategorySectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerIdentifier)
        collectionView.register(UINib(nibName: footerIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
}

extension CategoryViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return category.faultSections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.faultSections[section].faults.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = category.faultSections[indexPath.section]
        
        if indexPath.row == 0 || indexPath.row == section.faults.count + 1 {
            let marginCell = collectionView.dequeueReusableCell(withReuseIdentifier: marginIdentifier, for: indexPath)
            marginCell.contentView.backgroundColor = Style.tintColor(for: category.type)
            
            marginCell.layer.mask = nil
            var corners: UIRectCorner?
            if indexPath.row == 0 {
                corners = [.topLeft, .topRight]
            }
            if indexPath.row == section.faults.count + 1 {
                corners = [.bottomLeft, .bottomRight]
            }
            if let corners = corners {
                let size = CGSize(width: collectionView.frame.width, height: 13.0)
                let path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: size),
                                        byRoundingCorners: corners,
                                        cornerRadii: CGSize(width: 4.0, height: 4.0))
                let mask = CAShapeLayer()
                mask.path = path.cgPath
                marginCell.layer.mask = mask
            }
            
            return marginCell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! FaultCollectionViewCell
        
        cell.smoothColor = backgroundColor
        cell.backgroundColor = Style.tintColor(for: category.type)
        cell.tintColor = Style.textColor(for: category.type)
        
        let faultViewModel = sections[section.name!]![indexPath.row - 1]
        cell.configure(viewModel: faultViewModel,
                       onAdd: { [unowned self] in
            self.performSegue(withIdentifier: "showFaultDetails", sender: indexPath)
            }, onCheck: { [unowned self] in
                let state = faultViewModel.fault.state
                
                guard state != .fault else { return }
                
                faultViewModel.fault.state = state == nil ? .ok : nil
                if faultViewModel.fault.state == .ok {
                    self.graph.checkFault(category: self.category, sectionName: nil, faultName: faultViewModel.fault.name)
                } else {
                    self.graph.uncheckFault(category: self.category, sectionName: nil, faultName: faultViewModel.fault.name)
                }
                self.collectionView.reloadItems(at: [indexPath])
            }, onExpand: { [unowned self, faultViewModel] expandedHeight in
                faultViewModel.expanded.toggle()
                faultViewModel.expandedHeight = expandedHeight
                UIView.performWithoutAnimation {
                    self.collectionView.reloadItems(at: [indexPath])
                }
        })
        
        var height: CGFloat = faultViewModel.height
        if faultViewModel.expanded {
            height += faultViewModel.expandedHeight
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerIdentifier, for: indexPath) as! CategorySectionHeaderView
            
            header.tintColor = Style.textColor(for: category.type)
            let section = category.faultSections[indexPath.section].name ?? ""
            header.label.text = section
            header.label.font = .centuryGothic(withSize: 18.0)
            
            return header
        } else if indexPath.section == category.faultSections.count - 1 {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerIdentifier, for: indexPath) as! AddFaultFooterView
            
            footer.configure(color: accentColor,
                             onAdd: { [unowned self] in
                                self.performSegue(withIdentifier: "showFaultDetails", sender: nil)
                },
                             onDone: { [unowned self] in
                                self.onDone?()
            })
            
            return footer
        }
        
        return UICollectionReusableView()
    }
    
}

extension CategoryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let section = category.faultSections[indexPath.section]
        
        if indexPath.row == 0 || indexPath.row == section.faults.count + 1 { return CGSize(width: collectionView.frame.width, height: 13.0) }
        
        let faultViewModel = sections[section.name!]![indexPath.row - 1]
        var height: CGFloat = faultViewModel.height
        if faultViewModel.expanded {
            height += faultViewModel.expandedHeight
        }
        
        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: section == category.faultSections.count - 1 ? 190.0 : 0.0)
    }
    
}
