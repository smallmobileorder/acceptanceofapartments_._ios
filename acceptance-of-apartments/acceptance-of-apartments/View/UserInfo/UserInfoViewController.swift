//
//  UserInfoViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 26/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {
    
    var graph: Graph!
    var faults: [Fault] = []
    var excludedFaults: [Fault] = []
    var isSecond = false
    
    @IBOutlet private weak var infoStackView: UIStackView!
    @IBOutlet private weak var nameTextField: UITextField!
    private var nameTextFields: [UITextField] = []
    @IBOutlet private weak var complexTextField: UITextField!
    @IBOutlet private weak var addressTextField: UITextField!
    @IBOutlet private weak var contractNumberTextField: UITextField!
    @IBOutlet private weak var contractDateTextField: DatePickerTextField!
    @IBOutlet private weak var appartmentNumberLabel: UILabel!
    @IBOutlet private weak var appartmentNumberTextField: UITextField!
    @IBOutlet private weak var phoneNumberTextField: UITextField!
    @IBOutlet private weak var skipButton: UIButton!
    @IBOutlet private weak var saveButton: UIButton!
    
    private var isValid: Bool = false {
        didSet {
            saveButton.isEnabled = isValid
            if isValid {
                saveButton.backgroundColor = UIColor(red: 18, green: 111, blue: 67)
            } else {
                saveButton.backgroundColor = UIColor(red: 112, green: 112, blue: 112)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch graph.inspection.settings!.object {
        case .apartment: appartmentNumberLabel.text = "Номер квартиры"
        case .placement: appartmentNumberLabel.text = "Номер помещения"
        case .parking: appartmentNumberLabel.text = "Номер парковочного места"
        }
        
        saveButton.layer.cornerRadius = 12.0
        
        for textField in [nameTextField,
                          complexTextField,
                          addressTextField,
                          contractNumberTextField,
                          contractDateTextField,
                          appartmentNumberTextField,
                          phoneNumberTextField] {
            textField?.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
        
        nameTextFields.append(nameTextField)
        
        phoneNumberTextField.delegate = self
        
        isValid = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        skipButton.isHidden = graph.inspection.userInfo != nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navVC = segue.destination as? UINavigationController,
            let resultVC = navVC.viewControllers.first as? ResultViewController {
            resultVC.graph = graph
            resultVC.faults = faults
            resultVC.excludedFaults = excludedFaults
            resultVC.isSecond = isSecond
        }
    }
    
    @IBAction func addNewUser(_ sender: Any) {
        let themeColor = UIColor(red: 125, green: 114, blue: 114)
        
        let label = UILabel()
        label.text = "Дольщик"
        label.font = .centuryGothic(withSize: 12.0)
        label.textColor = themeColor
        
        let textField = UITextField()
        textField.placeholder = "Иванов Иван Иванович"
        textField.font = .centuryGothic(withSize: 16.0)
        textField.textColor = themeColor
        textField.autocapitalizationType = .words
        if #available(iOS 10.0, *) { textField.textContentType = .name }
        NSLayoutConstraint.activate([
            textField.heightAnchor.constraint(equalToConstant: 32.0)
        ])
        nameTextFields.append(textField)
        
        let splitterView = UIView()
        splitterView.backgroundColor = themeColor
        NSLayoutConstraint.activate([
            splitterView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        let stackView = UIStackView(arrangedSubviews: [label, textField, splitterView])
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        
        infoStackView.insertArrangedSubview(stackView, at: nameTextFields.count - 1)
    }
    
    @IBAction func skip(_ sender: Any) {
        performSegue(withIdentifier: "showResultSegue", sender: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        graph.addUserInfo(names: nameTextFields.compactMap { $0.text }.filter { !$0.isEmpty },
                          complex: complexTextField.text ?? "",
                          address: addressTextField.text ?? "",
                          contractNumber: contractNumberTextField.text ?? "",
                          contractDate: contractDateTextField.date!,
                          appartmentNumber: appartmentNumberTextField.text ?? "",
                          phoneNumber: phoneNumberTextField.text ?? "")
        
        performSegue(withIdentifier: "showResultSegue", sender: nil)
    }
    
}

extension UserInfoViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            textField.text = String(textField.text?.prefix(12) ?? "")
        }
        isValid =
            nameTextFields.contains(where: { !($0.text?.isEmpty ?? true) }) &&
            !(complexTextField.text?.isEmpty ?? true) &&
            !(addressTextField.text?.isEmpty ?? true) &&
            !(contractNumberTextField.text?.isEmpty ?? true) &&
            !(contractDateTextField.text?.isEmpty ?? true) &&
            !(appartmentNumberTextField.text?.isEmpty ?? true) &&
            phoneNumberTextField.text?.count == 12
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            textField.text = "+7"
        }
    }
    
}
