//
//  MenuViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 07/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet private weak var nextInspectionButton: UIButton!
    @IBOutlet private weak var myInspectionButton: UIButton!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let myInspectionsVC = segue.destination as? MyInspectionsViewController,
            let button = sender as? UIButton {
            myInspectionsVC.isSecond = button === nextInspectionButton
        } else if let infoVC = segue.destination as? InfoViewController {
            infoVC.type = .help
        }
    }
    
    @IBAction func unwindToMenu(segue:UIStoryboardSegue) { }

}

