//
//  InfoViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 02/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import Lightbox

class InfoViewController: UIViewController {
    
    var type: InfoType!
    var sections: [SectionViewModel<InfoSection>] = []

    @IBOutlet private weak var headerBar: BrandHeaderView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    private let headerIdentifier = String(describing: InfoHeaderTableViewCell.self)
    private let textIdentifier = String(describing: InfoTextTableViewCell.self)
    private let imagesIdentifier = String(describing: InfoImagesTableViewCell.self)
    
    private var cachedHeights: [IndexPath: CGFloat] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sections = InfoFactory.make(type: type)
        
        headerBar.image = Style.icon(for: type)
        
        titleLabel.text = type.rawValue
        
        tableView.register(UINib(nibName: headerIdentifier, bundle: nil), forCellReuseIdentifier: headerIdentifier)
        tableView.register(UINib(nibName: textIdentifier, bundle: nil), forCellReuseIdentifier: textIdentifier)
        tableView.register(UINib(nibName: imagesIdentifier, bundle: nil), forCellReuseIdentifier: imagesIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension InfoViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionViewModel = sections[section]
        let section = sectionViewModel.section
        return sectionViewModel.expanded ? section.items.count + 1 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionViewModel = sections[indexPath.section]
        let section = sectionViewModel.section
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: headerIdentifier, for: indexPath) as! InfoHeaderTableViewCell
            
            cell.configure(header: section.section)
            cell.onExpand = { [unowned self, sectionViewModel] in
                guard sectionViewModel.expand() else { return }
                
                let indexPaths = (1 ... sectionViewModel.section.items.count).map { IndexPath(row: $0, section: indexPath.section) }
                
                self.tableView.beginUpdates()
                if sectionViewModel.expanded {
                    self.tableView.insertRows(at: indexPaths, with: .fade)
                } else {
                    self.tableView.deleteRows(at: indexPaths, with: .fade)
                }
                self.tableView.endUpdates()
            }
            
            return cell
        } else {
            let info = section.items[indexPath.row - 1]
            
            if let text = info.text {
                let cell = tableView.dequeueReusableCell(withIdentifier: textIdentifier) as! InfoTextTableViewCell
                
                cell.configure(text: text)
                
                return cell
            } else if let images = info.images {
                let cell = tableView.dequeueReusableCell(withIdentifier: imagesIdentifier) as! InfoImagesTableViewCell
                
                cell.configure(images: images)
                cell.onShow = { [unowned self] name in
                    let image = UIImage(named: name + "_big")!
                    let vc = LightboxController(images: [LightboxImage(image: image)], startIndex: 0)
                    LightboxConfig.CloseButton.enabled = false
                    LightboxConfig.PageIndicator.enabled = false
                    LightboxConfig.DeleteButton.enabled = false
                    self.present(vc, animated: true)
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
}

extension InfoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cachedHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(for: indexPath)
    }
    
}

private extension InfoViewController {
    
    func height(for indexPath: IndexPath) -> CGFloat {
        if let cachedHeight = cachedHeights[indexPath] { return cachedHeight }
        if indexPath.row == 0 {
            if sections[indexPath.section].section.section == nil { return 0.0 }
            else { return UITableView.automaticDimension }
        }
        let model = sections[indexPath.section].section.items[indexPath.row - 1]
        if let images = model.images {
            let pairs = images.pairs
            if pairs.count == 1 {
                let pair = pairs.first!
                let imageWidth = tableView.frame.width / 1.5
                let imageHeight = imageWidth * CGFloat(pair.height / pair.width)
                return imageHeight + 20.0
            } else {
                let width = tableView.frame.width - 16.0 - CGFloat(images.spacing!) * CGFloat(pairs.count - 1)
                let imagesWidth = CGFloat(pairs.map { $0.width }.reduce(0, +))
                let k = width / imagesWidth
                let maxHeight = pairs.map { CGFloat($0.height) * k }.max()!
                return maxHeight + 20.0
            }
        }
        return UITableView.automaticDimension
    }
    
}
