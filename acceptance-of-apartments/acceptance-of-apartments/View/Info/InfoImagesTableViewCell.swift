//
//  InfoImagesTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class InfoImagesTableViewCell: UITableViewCell {
    
    var onShow: ((String) -> ())?
    
    @IBOutlet var stackViews: [UIStackView]!
    @IBOutlet var nameLabels: [UILabel]!
    @IBOutlet var imageViews: [UIImageView]!
    var images: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    func configure(images: InfoImages) {
        self.images = images.pairs.map { $0.named }
        stackViews.forEach { $0.isHidden = true }
        
        for (index, pair) in images.pairs.enumerated() {
            stackViews[index].isHidden = false
            nameLabels[index].text = pair.title
            imageViews[index].image = UIImage(named: pair.named)
        }
    }
    
}

private extension InfoImagesTableViewCell {
    
    func setup() {
        for imageView in imageViews {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap(gestureRecognizer:)))
            imageView.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    @objc func didTap(gestureRecognizer: UITapGestureRecognizer) {
        let imageView = gestureRecognizer.view as! UIImageView
        let index = imageViews.index(of: imageView)!
        onShow?(images[index])
    }
    
}
