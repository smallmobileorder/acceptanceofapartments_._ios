//
//  InfoTextTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class InfoTextTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var contentContainerView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentContainerView.layer.cornerRadius = 10.0
        contentContainerView.layer.borderColor = UIColor(red: 125, green: 114, blue: 114).cgColor
        contentContainerView.layer.borderWidth = 1.0
    }
    
    func configure(text: String) {
        let attributedString = try! NSMutableAttributedString(data: text.data(using: .unicode)!,
                                                       options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        attributedString.setFontFace(font: .centuryGothic(withSize: 14.0),
                                     color: UIColor(red: 125, green: 114, blue: 114))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacingBefore = 0.0
        paragraphStyle.paragraphSpacing = 0.0
        attributedString.removeAttribute(.paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        
        contentLabel.attributedText = attributedString
    }
    
}
