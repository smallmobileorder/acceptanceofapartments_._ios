//
//  InfoHeaderTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class InfoHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var headerLabel: UILabel!
    
    var onExpand: (() -> ())?
    
    func configure(header: String?) {
        headerLabel.text = header
    }
    
    @IBAction func expand(_ sender: Any) {
        onExpand?()
    }
    
}
