//
//  CategoryListViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 12/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class CategoryListViewController: UIViewController {
    
    var graph: Graph!
    
    @IBOutlet private weak var timeProgressStackView: UIStackView!
    @IBOutlet private weak var timeProgressWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var timeProgressViewContainer: RoundedView!
    @IBOutlet private weak var completionProgressView: UIProgressView!
    @IBOutlet private weak var continuteButton: UIBarButtonItem!
    
    @IBOutlet private weak var doorCategoryImageView: UIImageView!
    @IBOutlet private weak var furnishCategoryImageView: UIImageView!
    @IBOutlet private weak var windowsCategoryImageView: UIImageView!
    @IBOutlet private weak var facadeCategoryImageView: UIImageView!
    @IBOutlet private weak var stainedGlassCategoryImageView: UIImageView!
    @IBOutlet private weak var plumbingCategoryImageView: UIImageView!
    @IBOutlet private weak var electricityCategoryImageView: UIImageView!
    
    @IBOutlet private weak var facadeUnavailableImageView: UIImageView!
    @IBOutlet private weak var stainedGlassUnavailableImageView: UIImageView!
    
    @IBOutlet private weak var doorCategoryView: RoundedView!
    @IBOutlet private weak var furnishCategoryView: RoundedView!
    @IBOutlet private weak var windowsCategoryView: RoundedView!
    @IBOutlet private weak var facadeCategoryView: RoundedView!
    @IBOutlet private weak var stainedGlassCategoryView: RoundedView!
    @IBOutlet private weak var plumbingCategoryView: RoundedView!
    @IBOutlet private weak var electricityCategoryView: RoundedView!
    
    private lazy var allCategories: Set<RoundedView> = [doorCategoryView,
                                                        furnishCategoryView,
                                                        windowsCategoryView,
                                                        facadeCategoryView,
                                                        stainedGlassCategoryView,
                                                        plumbingCategoryView,
                                                        electricityCategoryView]
    
    private var timer: TimerService?
    
    private lazy var shouldNotifyTenMinutes = graph.timeForInspection ?? 0 > 10
    private var shouldNotifyHalfTime = true
    private var shouldNotifyEndTime = true
    
    private var enabledCategories: [UIView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        graph.startInspection()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupIcons()
        completionProgressView.progress = graph.progress
        continuteButton.isEnabled = graph.isFinished
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navVC = segue.destination as? UINavigationController,
            let categoryVC = navVC.viewControllers.first as? CategoryViewController,
            let sender = sender as? UIView,
            let index = enabledCategories.firstIndex(of: sender) {
            categoryVC.graph = graph
            categoryVC.category = graph.categories[index]
            categoryVC.onDone = {
                categoryVC.dismiss(animated: true)
            }
        } else if let navVC = segue.destination as? UINavigationController,
            let resultVC = navVC.viewControllers.first as? InspectionResultViewController {
            resultVC.graph = graph
        }
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

extension CategoryListViewController: TimerServiceDelegate {
    
    func timerService(timerService: TimerService, secondsElapsed: Int) {
        let progress = CGFloat(secondsElapsed) / CGFloat(graph.timeForInspection!) / 60.0
        timeProgressWidthConstraint.constant = timeProgressViewContainer.frame.width  * (1.0 - progress)

        let secondsRemaining = graph.timeForInspection! * 60 - secondsElapsed
        var message: String?
        if secondsRemaining <= 600 && shouldNotifyTenMinutes {
            message = "Осталось 10 минут отведённого на осмотр времени"
            shouldNotifyTenMinutes = false
        }
        if progress >= 0.5 && shouldNotifyHalfTime {
            message = "Прошла половина отведённого на осмотр времени"
            shouldNotifyHalfTime = false
        }
        if secondsRemaining == 0 && shouldNotifyEndTime {
            message = "Закончилось основное отведённое на осмотр время"
            shouldNotifyEndTime = false
        }
        if let message = message {
            var topViewController: UIViewController = self
            while true {
                if let presented = topViewController.presentedViewController {
                    topViewController = presented
                } else { break }
            }
            topViewController.showAlert(title: "Внимание", message: message)
        }
    }
    
}

private extension CategoryListViewController {
    
    func setup() {
        setupTimer()
        styleUI()
        setupCategories()
    }
    
    func setupTimer() {
        guard let time = graph.timeForInspection else {
            timeProgressStackView.removeFromSuperview()
            return
        }
        
        timer = TimerService()
        timer?.delegate = self
        timer?.start(seconds: time * 60)
    }
    
    func styleUI() {
        for category in allCategories {
            category.setNeedsLayout()
            category.layoutIfNeeded()
            category.radius = category.frame.height / 3.0
        }
    }
    
    func setupCategories() {
        var disabledCategories = allCategories
        
        for category in graph.categories {
            var categoryView: UIView!
            
            switch category.type {
            case .door: categoryView = disabledCategories.remove(doorCategoryView)
            case .furnish: categoryView = disabledCategories.remove(furnishCategoryView)
            case .windows: categoryView = disabledCategories.remove(windowsCategoryView)
            case .facade: categoryView = disabledCategories.remove(facadeCategoryView)
            case .stainedGlass: categoryView = disabledCategories.remove(stainedGlassCategoryView)
            case .plumbing: categoryView = disabledCategories.remove(plumbingCategoryView)
            case .electricity: categoryView = disabledCategories.remove(electricityCategoryView)
            case .parking: continue
            }
            
            enabledCategories.append(categoryView)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCategory(tapGestureRecognizer:)))
            categoryView.addGestureRecognizer(tapGestureRecognizer)
        }
        
        let disabledColor = UIColor(red: 172, green: 165, blue: 165)
        for disabledCategory in disabledCategories {
            disabledCategory.backgroundColor = disabledColor
            
            if disabledCategory == facadeCategoryView {
                facadeCategoryImageView.image = #imageLiteral(resourceName: "facade_template_icon")
                facadeUnavailableImageView.isHidden = false
            } else if disabledCategory == stainedGlassCategoryView {
                stainedGlassCategoryImageView.image = #imageLiteral(resourceName: "stained_glass_template_icon")
                stainedGlassUnavailableImageView.isHidden = false
            }
        }
    }
    
    @objc func showCategory(tapGestureRecognizer: UITapGestureRecognizer) {
        performSegue(withIdentifier: "showCategory", sender: tapGestureRecognizer.view)
    }
    
    func setupIcons() {
        for category in graph.categories {
            switch category.type {
            case .door: doorCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .furnish: furnishCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .windows: windowsCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .facade: facadeCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .stainedGlass: stainedGlassCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .plumbing: plumbingCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .electricity: electricityCategoryImageView.image = category.isFinished ? #imageLiteral(resourceName: "done_icon") : Style.icon(for: category.type)
            case .parking: continue
            }
        }
    }
    
}
