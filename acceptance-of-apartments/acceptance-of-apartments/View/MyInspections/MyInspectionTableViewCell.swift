//
//  MyInspectionTableViewCell.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 26/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class MyInspectionTableViewCell: UITableViewCell {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var middleLabel: UILabel!
    @IBOutlet private weak var bottomLabel: UILabel!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 12.0
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor(red: 112, green: 112, blue: 112).cgColor
    }
    
    func configure(inspection: Inspection, isSecond: Bool) {
        var object: String
        switch inspection.settings!.object {
        case .apartment: object = "Квартира"
        case .placement: object = "Помещение"
        case .parking: object = "Парковочное место"
        }
        let dateString = inspection.date.formatted("dd.MM.yyyy")
        
        [topLabel, middleLabel, bottomLabel].forEach { $0?.isHidden = false }
        if isSecond {
            if let userInfo = inspection.userInfo {
                topLabel.text = "\(object) № \(userInfo.appartmentNumber)"
                middleLabel.text = "ЖК \"\(userInfo.complexInfo)\""
            } else {
                topLabel.text = object
                middleLabel.isHidden = true
            }
            bottomLabel.text = "Осмотр от \(dateString)"
        } else {
            if let repeatDate = inspection.repeatDate {
                topLabel.text = "Повторный осмотр от \(repeatDate.formatted("dd.MM.yyyy"))"
            } else {
                topLabel.text = "Первичный осмотр от \(dateString)"
            }
            if let userInfo = inspection.userInfo {
                bottomLabel.text = "ЖК \"\(userInfo.complexInfo)\""
                middleLabel.text = "\(object) № \(userInfo.appartmentNumber)"
            } else {
                bottomLabel.isHidden = true
                middleLabel.text = object
            }
        }
    }
    
}
