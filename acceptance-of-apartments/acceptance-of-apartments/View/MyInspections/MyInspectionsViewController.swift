//
//  MyInspectionsViewController.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 26/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit
import RealmSwift

class MyInspectionsViewController: UIViewController {
    
    var isSecond: Bool!
    
    private var inspections: [Inspection] = []
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    private let identifier = String(describing: MyInspectionTableViewCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let realm = try! Realm()
        inspections = Array(realm.objects(Inspection.self)).sorted(by: { left, right in
            return left.repeatDate ?? left.date > right.repeatDate ?? right.date
        })
        if isSecond {
            inspections = inspections.filter { inspection in
                guard inspection.repeatDate == nil,
                    !inspection.isStarted else { return false }
                
                var mostRecentInspection = inspection
                while let newest = mostRecentInspection.successor {
                    mostRecentInspection = newest
                }
                
                return mostRecentInspection.faults.contains(where: { !$0.checked && !$0.fixed })
            }
            titleLabel.text = "Выберите первичный осмотр для проверки устранения замечаний"
        } else {
            inspections = inspections.filter { $0.repeatDate == nil || !$0.isStarted  }
            titleLabel.text = "Выберите осмотр для отображения отчета"
        }
        
        for inspection in inspections {
            print("inspection", Unmanaged.passUnretained(inspection).toOpaque())
            if let successor = inspection.successor {
                print("successor", Unmanaged.passUnretained(successor).toOpaque())
            } else {
                print("successor: nil")
            }
        }
        
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let indexPath = sender as? IndexPath else { return }
        
        let inspection = inspections[indexPath.row]
        let graph = GraphFactory(inspection: inspection)!.create()
        
        if let faultsListVC = segue.destination as? FaultsListViewController {
            var mostRecentInspection = graph.inspection
            while let newest = mostRecentInspection.successor {
                if newest.isStarted { break }
                mostRecentInspection = newest
            }
//            var selectedInspection: Inspection
//            if mostRecentInspection.isStarted {
//                selectedInspection = mostRecentInspection
//            } else {
                let copy = mostRecentInspection.makeCopy()
                copy.repeatDate = Date()
                let realm = try! Realm()
                try! realm.write {
                    realm.add(copy)
                    copy.isMoreThanTwice = mostRecentInspection.repeatDate != nil
                    mostRecentInspection.successor = copy
                }
//                selectedInspection = copy
//            }
            
//            let selectedGraph = GraphFactory(inspection: selectedInspection)?.create()
            let selectedGraph = GraphFactory(inspection: copy)?.create()
            selectedGraph?.startInspection()
            faultsListVC.graph = selectedGraph
            faultsListVC.isSecond = true
        } else if let resultVC = segue.destination as? InspectionResultViewController {
            resultVC.graph = graph
            resultVC.isSecond = true
        } else if let finalVC = segue.destination as? ResultViewController {
            finalVC.graph = graph
            finalVC.isSecond = graph.inspection.repeatDate != nil
        } else if let navVC = segue.destination as? UINavigationController,
            let parkingCategoryVC = navVC.topViewController as? CategoryViewController {
            parkingCategoryVC.graph = graph
            parkingCategoryVC.category = graph.categories.first
            parkingCategoryVC.onDone = { [weak graph] in
                guard graph?.isFinished ?? true else { return }
                
                parkingCategoryVC.performSegue(withIdentifier: "showInspectionResultSegue", sender: nil)
            }
        } else if let navVC = segue.destination as? UINavigationController,
            let categoriesVC = navVC.topViewController as? CategoryListViewController {
            categoriesVC.graph = graph
        }
    }

}

extension MyInspectionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inspections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MyInspectionTableViewCell
        
        let inspection = inspections[indexPath.row]
        cell.configure(inspection: inspection, isSecond: isSecond)
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension MyInspectionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let inspection = inspections[indexPath.row]
        DispatchQueue.main.async {
            if self.isSecond {
                self.performSegue(withIdentifier: "showFaultsListSegue", sender: indexPath)
            } else {
                if inspection.isStarted {
                    if inspection.repeatDate != nil {
                        self.performSegue(withIdentifier: "showFaultsListSegue", sender: indexPath)
                    } else {
                        switch inspection.settings!.object {
                        case .parking: self.performSegue(withIdentifier: "continueParkingFirstInspectionSegue", sender: indexPath)
                        default: self.performSegue(withIdentifier: "continueFirstInspectionSegue", sender: indexPath)
                        }
                    }
                } else if inspection.repeatDate == nil {
                    self.performSegue(withIdentifier: "showResultSegue", sender: indexPath)
                } else {
                    self.performSegue(withIdentifier: "showFinalSegue", sender: indexPath)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Подтвердите", message: "Удалить осмотр?", preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Удалить", style: .destructive) { [unowned self] _ in
                let realm = try! Realm()
                if self.isSecond {
                    let inspection = self.inspections[indexPath.row]
                    var mostRecentInspection = inspection
                    while let newest = mostRecentInspection.successor {
                        if newest.isStarted { break }
                        mostRecentInspection = newest
                    }
                    let shouldRemoveFromList = mostRecentInspection.uuid == inspection.uuid
                    try! realm.write {
                        realm.delete(mostRecentInspection)
                    }
                    if shouldRemoveFromList {
                        let _ = self.inspections.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: .automatic);
                    }
                } else {
                    let inspection = self.inspections.remove(at: indexPath.row)
                    let predecessor = realm.objects(Inspection.self).first(where: { $0.successor?.uuid == inspection.uuid })
                    try! realm.write {
                        predecessor?.successor = inspection.successor
                        realm.delete(inspection)
                    }
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            alert.addAction(confirmAction)
            alert.addAction(UIAlertAction(title: "Отменить", style: .cancel))
            present(alert, animated: true)
        }
    }
    
}
