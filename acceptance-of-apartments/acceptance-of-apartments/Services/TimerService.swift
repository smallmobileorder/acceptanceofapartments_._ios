//
//  TimerService.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 24/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

protocol TimerServiceDelegate: class {
    func timerService(started: TimerService)
    func timerService(timerService: TimerService, secondsElapsed: Int)
    func timerService(finished: TimerService)
}

extension TimerServiceDelegate {
    func timerService(started: TimerService) { }
    func timerService(timerService: TimerService, secondsElapsed: Int) { }
    func timerService(finished: TimerService) { }
}

class TimerService {
    
    weak var delegate: TimerServiceDelegate?
    
    private(set) var isRunning = false {
        didSet {
            didUpdateState()
        }
    }
    
    private var startDate: Date!
    private var timer: Timer?
    
    private(set) var desiredSeconds: Int!
    private var secondsElapsed = 0 {
        didSet {
            delegate?.timerService(timerService: self, secondsElapsed: secondsElapsed)
        }
    }
    
    func start(seconds: Int) {
        guard !isRunning else {
            return
        }
        
        desiredSeconds = seconds
        isRunning = true
        
        timer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(onTick),
                                     userInfo: nil,
                                     repeats: true)
    }
    
}

private extension TimerService {
    
    @objc func onTick() {
        guard isRunning else {
            return
        }
        
        let currentDate = Date()
        secondsElapsed = min(desiredSeconds, Int(currentDate.timeIntervalSince(startDate)))
        if secondsElapsed == desiredSeconds {
            isRunning = false
        }
    }
    
    func didUpdateState() {
        if isRunning {
            secondsElapsed = 0
            startDate = Date()
            delegate?.timerService(started: self)
        } else {
            timer?.invalidate()
            timer = nil
            delegate?.timerService(finished: self)
        }
    }
    
}
