//
//  GraphFactrory.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 09/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

struct GraphFactory {
    
    private let inspection: Inspection
    
    let placements: [String]
    let inspectionTime: Int?
    let hasFacade: Bool
    let hasStainedGlass: Bool
    let objectType: ObjectType
    let furnishType: FurnishType
    
    func create() -> Graph {
        var categories: [GraphCategory] = []
        
        var furnishKey = ""
        switch furnishType {
        case .full: furnishKey = "Чистовая"
        case .preparing: furnishKey = "Предчистовая"
        case .none: furnishKey = "Черновая"
        }
        
        var totalWeight: Int
        
        switch objectType {
        case .apartment(_, _, _, _, _):
            let json = Bundle.main.json(named: "appartment")
            let dict = try! JSONDecoder().decode([String: [GeneratedFaultSection]].self, from: json)
            
            let doorGeneratedSections = dict["Входная дверь"]!
            let doorCategory = createCategory(type: .door,
                                              generatedSections: doorGeneratedSections,
                                              insertPlacements: ["Коридор"])
            categories.append(doorCategory)
            
            let furnishGeneratedSections = dict["Отделка (\(furnishKey))"]!
            let furnishCategory = createCategory(type: .furnish,
                                                 generatedSections: furnishGeneratedSections,
                                                 insertPlacements: placements)
            categories.append(furnishCategory)
            
            var windowsGeneratedSections: [GeneratedFaultSection]
            if furnishType != .full {
                windowsGeneratedSections = dict["Окна (Черновая или Предчистовая)"]!
            } else {
                windowsGeneratedSections = dict["Окна (Чистовая)"]!
            }
            let windowsCategory = createCategory(type: .windows,
                                                 generatedSections: windowsGeneratedSections,
                                                 insertPlacements: placements)
            categories.append(windowsCategory)
            
            if hasFacade {
                let facadeGeneratedSections = dict["Фасад"]!
                let facadeCategory = createCategory(type: .facade,
                                                    generatedSections: facadeGeneratedSections,
                                                    insertPlacements: ["Балкон"])
                categories.append(facadeCategory)
            }
            
            if hasStainedGlass {
                let stainedGeneratedSections = dict["Витражи"]!
                let stainedCategory = createCategory(type: .stainedGlass,
                                                     generatedSections: stainedGeneratedSections,
                                                     insertPlacements: ["Балкон"])
                categories.append(stainedCategory)
            }
            
            var plumbingGeneratedSections: [GeneratedFaultSection]
            if furnishType != .full {
                plumbingGeneratedSections = dict["Сантехника (Черновая или Предчистовая)"]!
            } else {
                plumbingGeneratedSections = dict["Сантехника (Чистовая)"]!
            }
            let plumbingCategory = createCategory(type: .plumbing,
                                                  generatedSections: plumbingGeneratedSections,
                                                  insertPlacements: placements)
            categories.append(plumbingCategory)
            
            let electricityGeneratedSections = dict["Электрика"]!
            let electricityCategory = createCategory(type: .electricity,
                                                     generatedSections: electricityGeneratedSections,
                                                     insertPlacements: placements)
            categories.append(electricityCategory)
            
            switch furnishType {
            case .full: totalWeight = 1698
            case .preparing: totalWeight = 1111
            case .none: totalWeight = 995
            }
            
        case .placement(_, _):
            let json = Bundle.main.json(named: "commercy")
            let dict = try! JSONDecoder().decode([String: [GeneratedFaultSection]].self, from: json)
            
            let doorGeneratedSections = dict["Входная дверь"]!
            let doorCategory = createCategory(type: .door,
                                              generatedSections: doorGeneratedSections,
                                              insertPlacements: placements)
            categories.append(doorCategory)
            
            let furnishGeneratedSections = dict["Отделка (\(furnishKey))"]!
            let furnishCategory = createCategory(type: .furnish,
                                                 generatedSections: furnishGeneratedSections,
                                                 insertPlacements: placements)
            categories.append(furnishCategory)
            
            let windowsGeneratedSections = dict["Окна"]!
            let windowsCategory = createCategory(type: .windows,
                                                 generatedSections: windowsGeneratedSections,
                                                 insertPlacements: placements)
            categories.append(windowsCategory)
            
            let facadeGeneratedSections = dict["Фасад"]!
            let facadeCategory = createCategory(type: .facade,
                                                generatedSections: facadeGeneratedSections,
                                                insertPlacements: placements)
            categories.append(facadeCategory)
            
            let plumbingGeneratedSections = dict["Сантехника"]!
            let plumbingCategory = createCategory(type: .plumbing,
                                                  generatedSections: plumbingGeneratedSections,
                                                  insertPlacements: placements)
            categories.append(plumbingCategory)
            
            let electricityGeneratedSections = dict["Электрика"]!
            let electricityCategory = createCategory(type: .electricity,
                                                     generatedSections: electricityGeneratedSections,
                                                     insertPlacements: placements)
            categories.append(electricityCategory)
            switch furnishType {
            case .full: fatalError()
            case .preparing: totalWeight = 1111
            case .none: totalWeight = 995
            }
            
        case .parking:
            let json = Bundle.main.json(named: "parking")
            let dict = try! JSONDecoder().decode([String: [GeneratedFaultSection]].self, from: json)
            
            let parkingGeneratedSections = dict["Парковочное место"]!
            let parkingCategory = createCategory(type: .parking,
                                                 generatedSections: parkingGeneratedSections,
                                                 insertPlacements: ["Парковочное место"])
            categories.append(parkingCategory)
            
            totalWeight = 134
        }
        
        return Graph(categories: categories, inspection: inspection, placements: Set(placements), timeForInspection: inspectionTime, totalWeight: totalWeight)
    }
    
}

private extension GraphFactory {
    
    func createCategory(type: GraphCategory.CategoryType, generatedSections: [GeneratedFaultSection], insertPlacements: [String]) -> GraphCategory {
        var sections: [GraphFaultSection] = []
        for section in generatedSections {
            var faults: [GraphFault] = []
            for fault in section.listRemark {
                var graphDetails: [GraphDetail] = []

                for pair in fault.mapPlaceRemark where pair.key != "Помещение" && !pair.value.isEmpty {
                    graphDetails.append(.details(title: pair.key, options: pair.value.compactMap { $0.remark }))
                }
                
                graphDetails.append(.placements(title: "Выберите помещение", options: insertPlacements))
                
                faults.append(createFault(name: fault.remark, details: graphDetails, comment: fault.comment, currentSubRemark: fault.currentSubRemark, mapPlaceRemark: fault.mapPlaceRemark, docs: fault.docs))
            }
            sections.append(GraphFaultSection(name: section.group, faults: faults))
        }
        
        return GraphCategory(type: type, faultSections: sections, inspection: inspection)
    }
    
    func createFault(name: String, details: [GraphDetail], comment: String, currentSubRemark: GeneratedInfo, mapPlaceRemark: [String: [GeneratedInfo]], docs: [String]) -> GraphFault {
        return GraphFault(name: name, details: details, inspection: inspection, comment: comment, currentSubRemark: currentSubRemark, mapPlaceRemark: mapPlaceRemark, docs: docs)
    }
    
}

extension GraphFactory {
    
    init?(inspection: Inspection) {
        guard let settings = inspection.settings else { return nil }
        
        self.inspection = inspection
        
        var selectedPlacements: [String] = []
        var facadeActivated = false
        var stainedGlassActivated = false
        
        switch settings.object {
        case .apartment(let numberOfRooms, let combinedBathroom, let larder, let balcony, let minutes):
            if numberOfRooms == 0 {
                selectedPlacements.append("Комната")
            } else {
                for roomIndex in 1 ... numberOfRooms {
                    selectedPlacements.append("Комната \(roomIndex)")
                }
                selectedPlacements.append("Кухня")
            }
            
            
            if larder {
                selectedPlacements.append("Кладовка")
            }
            
            selectedPlacements.append("Коридор")
            
            facadeActivated = balcony
            stainedGlassActivated = balcony
            if balcony {
                selectedPlacements.append("Балкон")
            }
            
            if combinedBathroom {
                selectedPlacements.append("С/У")
            } else {
                selectedPlacements.append(contentsOf: ["Ванная", "Туалет"])
            }
            
            inspectionTime = minutes
            
        case .placement(let numberOfPlacements, let minutes):
            for placementIndex in 1 ... numberOfPlacements {
                selectedPlacements.append("Помещение \(placementIndex)")
            }
            
            facadeActivated = true
            
            inspectionTime = minutes
            
        case .parking:
            selectedPlacements.append("Парковочное место")
            
            inspectionTime = nil
        }
        
        placements = selectedPlacements
        hasFacade = facadeActivated
        hasStainedGlass = stainedGlassActivated
        objectType = settings.object
        furnishType = settings.furnishType
    }
    
}
