//
//  InfoFactory.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 04/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation
class SectionViewModel<T> {
    
    let section: T
    let expandable: Bool
    private(set) var expanded: Bool
    
    init(section: T, expandable: Bool) {
        self.section = section
        self.expandable = expandable
        expanded = !expandable
    }
    
    func expand() -> Bool {
        guard expandable else { return false }
        
        expanded.toggle()
        
        return true
    }
    
}

class InfoFactory {
    
    static func make(type: InfoType) -> [SectionViewModel<InfoSection>] {
        let json = Bundle.main.json(named: type.rawValue)
        let dict = try! JSONDecoder().decode([InfoSection].self, from: json)
        return dict.map { SectionViewModel(section: $0, expandable: type == .help) }
    }
    
}
