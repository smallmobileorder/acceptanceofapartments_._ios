//
//  InspectionFactory.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 13/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation
import RealmSwift

struct InspectionFactory {
    
    func create(objectType: ObjectType, furnishType: FurnishType) -> Inspection {
        let realm = try! Realm()
        let settings = InspectionSettings(object: objectType, furnishType: furnishType)
        let inspection = Inspection()
        try! realm.write {
            realm.add(inspection)
            inspection.settings = settings
        }
        return inspection
    }
    
}
