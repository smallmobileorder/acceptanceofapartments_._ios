//
//  PhotoManager.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 21/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

class PhotoManager {
    
    private static var fileManager: FileManager { return FileManager.default }
    
    static func save(photo: UIImage?) -> String? {
        guard let photo = photo,
            let path = fileManager.documentsDirectory,
            let data = photo.jpegData(compressionQuality: 0.85) else { return nil }
        
        let name = "/\(UUID().uuidString).jpg"
        
        if fileManager.createFile(atPath: path.appending(name), contents: data, attributes: nil) {
            return name
        } else {
            return nil
        }
    }
    
    static func get(name: String?) -> UIImage? {
        guard let name = name,
            let path = fileManager.documentsDirectory else { return nil }
        
        return UIImage(contentsOfFile: path.appending(name))
    }
    
    static func delete(name: String) {
        guard let path = fileManager.documentsDirectory else { return }
        
        try? fileManager.removeItem(atPath: path.appending(name))
    }
    
}
