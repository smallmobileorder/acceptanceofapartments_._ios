//
//  UIButton+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 02/05/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension UIButton: Descriptable {
    
    var name: String {
        return title(for: .normal) ?? ""
    }
    
}
