//
//  UIColor+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 17/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int, alpha: Int = 100) {
        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: CGFloat(alpha) / 100.0)
    }
    
    func lighter(percent: CGFloat) -> UIColor? {
        return brightness(by: percent)
    }

    func darker(percent: CGFloat) -> UIColor? {
        return brightness(by: -percent)
    }
    
    private func brightness(by percent: CGFloat) -> UIColor {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else { return self }
        
        return UIColor(hue: h,
                       saturation: s,
                       brightness: (b + percent).clamped(0.0, 1.0),
                       alpha: a)
    }

}
