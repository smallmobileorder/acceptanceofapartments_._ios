//
//  UIView+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 19/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension UIView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColorDidChange()
    }
    
    func round(with corners: UIRectCorner, radius: CGFloat = 5.0) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }

    
    func setDismissKeyboardOnTap() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard() {
        endEditing(true)
    }
    
}
