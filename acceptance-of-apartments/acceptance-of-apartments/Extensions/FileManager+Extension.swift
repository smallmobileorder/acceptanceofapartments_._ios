//
//  FileManager+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 21/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

extension FileManager {
    
    var documentsDirectory: String? {
        return urls(for: .documentDirectory, in: .userDomainMask).first?.path
    }
    
}
