//
//  UIFont+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 20/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func centuryGothic(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "Century Gothic", size: size)!
    }
    
    static func calibri(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "Calibri", size: size)!
    }
    
    func withTraits(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
}
