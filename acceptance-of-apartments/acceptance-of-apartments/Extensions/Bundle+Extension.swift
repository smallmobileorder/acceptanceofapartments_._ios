//
//  Bundle+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 25/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

extension Bundle {
    
    func json(named name: String) -> Data {
        let jsonPath = path(forResource: name, ofType: "json")!
        return try! Data(contentsOf: URL(fileURLWithPath: jsonPath))
    }
    
}
