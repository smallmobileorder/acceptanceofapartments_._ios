//
//  CGFloat+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 17/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension CGFloat {
    
    func clamped(_ from: CGFloat, _ to: CGFloat) -> CGFloat {
        return CGFloat.minimum(CGFloat.maximum(from, self), to)
    }
    
}
