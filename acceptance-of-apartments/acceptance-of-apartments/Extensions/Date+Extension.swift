//
//  Date+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 27/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import Foundation

extension Date {
    
    func formatted(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
