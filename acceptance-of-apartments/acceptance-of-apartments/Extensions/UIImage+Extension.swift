//
//  UIImage+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 28/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import UIKit

extension UIImage {
    
    var normalized: UIImage? {
        if (imageOrientation == .up) { return self }
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale);
        defer { UIGraphicsEndImageContext() }
        
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}
