//
//  List+Extension.swift
//  acceptance-of-apartments
//
//  Created by Eldar Goloviznin on 19/04/2019.
//  Copyright © 2019 Eldar Goloviznin. All rights reserved.
//

import RealmSwift

extension List {
    
    var array: Array<Element> {
        return Array(self)
    }
    
}

extension List where Element: Hashable {
    
    var set: Set<Element> {
        return Set(self)
    }
    
}
