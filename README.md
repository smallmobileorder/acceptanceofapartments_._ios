# Приемка квартир
Приложение позволяет производить процесс приемки квартиры, по большому кол-ву заготовленных случаев на которые стоит обращать внимание при приеме квартиры. Отслеживать исправление замечаний и формировать отчет приема квартир в формате Excel. 
## Скриншоты:
<img src="screenshoots/screen__9_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__11_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__12_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__13_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__14_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__16_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__18_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__19_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__3_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__20_.png" alt="drawing" width="200"/>
<img src="screenshoots/screen__22_.png" alt="drawing" width="200"/>